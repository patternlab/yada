﻿namespace PatternTools.MSParser.Deisotoping
{
    partial class SpectrumFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpectrumFilter));
            this.kryptonRichTextBoxIn = new ComponentFactory.Krypton.Toolkit.KryptonRichTextBox();
            this.kryptonRichTextBoxOut = new ComponentFactory.Krypton.Toolkit.KryptonRichTextBox();
            this.kryptonButtonConvert = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownMinMSIntensity = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonNumericUpDownPPM = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabelTotalPeaksLeft = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabelTotalPeaks = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabelTotalPeaksRight = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonButtonClear = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBoxSpectrumViewer = new System.Windows.Forms.GroupBox();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.msViewer2 = new PatternTools.SpectrumViewer.MSViewer();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonCheckBoxGaussianFilter = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonLabelGFCutoff = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxSpectrumViewer.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // kryptonRichTextBoxIn
            // 
            this.kryptonRichTextBoxIn.Location = new System.Drawing.Point(6, 19);
            this.kryptonRichTextBoxIn.Name = "kryptonRichTextBoxIn";
            this.kryptonRichTextBoxIn.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Both;
            this.kryptonRichTextBoxIn.Size = new System.Drawing.Size(220, 73);
            this.kryptonRichTextBoxIn.TabIndex = 0;
            // 
            // kryptonRichTextBoxOut
            // 
            this.kryptonRichTextBoxOut.Location = new System.Drawing.Point(305, 19);
            this.kryptonRichTextBoxOut.Name = "kryptonRichTextBoxOut";
            this.kryptonRichTextBoxOut.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Both;
            this.kryptonRichTextBoxOut.Size = new System.Drawing.Size(220, 73);
            this.kryptonRichTextBoxOut.TabIndex = 1;
            // 
            // kryptonButtonConvert
            // 
            this.kryptonButtonConvert.Location = new System.Drawing.Point(232, 19);
            this.kryptonButtonConvert.Name = "kryptonButtonConvert";
            this.kryptonButtonConvert.Size = new System.Drawing.Size(67, 73);
            this.kryptonButtonConvert.TabIndex = 2;
            this.kryptonButtonConvert.Text = "->";
            this.kryptonButtonConvert.Values.ExtraText = "";
            this.kryptonButtonConvert.Values.Image = null;
            this.kryptonButtonConvert.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonConvert.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonConvert.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonConvert.Values.Text = "->";
            this.kryptonButtonConvert.Click += new System.EventHandler(this.kryptonButtonConvert_Click);
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(3, 9);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(96, 20);
            this.kryptonLabel1.TabIndex = 3;
            this.kryptonLabel1.Text = "Hard cutoff Fltr:";
            this.kryptonLabel1.Values.ExtraText = "";
            this.kryptonLabel1.Values.Image = null;
            this.kryptonLabel1.Values.Text = "Hard cutoff Fltr:";
            // 
            // kryptonNumericUpDownMinMSIntensity
            // 
            this.kryptonNumericUpDownMinMSIntensity.Location = new System.Drawing.Point(105, 9);
            this.kryptonNumericUpDownMinMSIntensity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMinMSIntensity.Name = "kryptonNumericUpDownMinMSIntensity";
            this.kryptonNumericUpDownMinMSIntensity.Size = new System.Drawing.Size(104, 22);
            this.kryptonNumericUpDownMinMSIntensity.TabIndex = 4;
            this.kryptonNumericUpDownMinMSIntensity.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // kryptonNumericUpDownPPM
            // 
            this.kryptonNumericUpDownPPM.Location = new System.Drawing.Point(121, 114);
            this.kryptonNumericUpDownPPM.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.kryptonNumericUpDownPPM.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.kryptonNumericUpDownPPM.Name = "kryptonNumericUpDownPPM";
            this.kryptonNumericUpDownPPM.Size = new System.Drawing.Size(88, 22);
            this.kryptonNumericUpDownPPM.TabIndex = 5;
            this.kryptonNumericUpDownPPM.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(3, 116);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(114, 20);
            this.kryptonLabel2.TabIndex = 6;
            this.kryptonLabel2.Text = "Monotonl Fltr PPM";
            this.kryptonLabel2.Values.ExtraText = "";
            this.kryptonLabel2.Values.Image = null;
            this.kryptonLabel2.Values.Text = "Monotonl Fltr PPM";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(6, 98);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(96, 20);
            this.kryptonLabel3.TabIndex = 7;
            this.kryptonLabel3.Text = "Total peaks left: ";
            this.kryptonLabel3.Values.ExtraText = "";
            this.kryptonLabel3.Values.Image = null;
            this.kryptonLabel3.Values.Text = "Total peaks left: ";
            // 
            // kryptonLabelTotalPeaksLeft
            // 
            this.kryptonLabelTotalPeaksLeft.Location = new System.Drawing.Point(108, 98);
            this.kryptonLabelTotalPeaksLeft.Name = "kryptonLabelTotalPeaksLeft";
            this.kryptonLabelTotalPeaksLeft.Size = new System.Drawing.Size(17, 20);
            this.kryptonLabelTotalPeaksLeft.TabIndex = 8;
            this.kryptonLabelTotalPeaksLeft.Text = "0";
            this.kryptonLabelTotalPeaksLeft.Values.ExtraText = "";
            this.kryptonLabelTotalPeaksLeft.Values.Image = null;
            this.kryptonLabelTotalPeaksLeft.Values.Text = "0";
            // 
            // kryptonLabelTotalPeaks
            // 
            this.kryptonLabelTotalPeaks.Location = new System.Drawing.Point(305, 98);
            this.kryptonLabelTotalPeaks.Name = "kryptonLabelTotalPeaks";
            this.kryptonLabelTotalPeaks.Size = new System.Drawing.Size(104, 20);
            this.kryptonLabelTotalPeaks.TabIndex = 9;
            this.kryptonLabelTotalPeaks.Text = "Total peaks right:";
            this.kryptonLabelTotalPeaks.Values.ExtraText = "";
            this.kryptonLabelTotalPeaks.Values.Image = null;
            this.kryptonLabelTotalPeaks.Values.Text = "Total peaks right:";
            // 
            // kryptonLabelTotalPeaksRight
            // 
            this.kryptonLabelTotalPeaksRight.Location = new System.Drawing.Point(423, 98);
            this.kryptonLabelTotalPeaksRight.Name = "kryptonLabelTotalPeaksRight";
            this.kryptonLabelTotalPeaksRight.Size = new System.Drawing.Size(17, 20);
            this.kryptonLabelTotalPeaksRight.TabIndex = 10;
            this.kryptonLabelTotalPeaksRight.Text = "0";
            this.kryptonLabelTotalPeaksRight.Values.ExtraText = "";
            this.kryptonLabelTotalPeaksRight.Values.Image = null;
            this.kryptonLabelTotalPeaksRight.Values.Text = "0";
            // 
            // kryptonButtonClear
            // 
            this.kryptonButtonClear.Location = new System.Drawing.Point(232, 98);
            this.kryptonButtonClear.Name = "kryptonButtonClear";
            this.kryptonButtonClear.Size = new System.Drawing.Size(67, 25);
            this.kryptonButtonClear.TabIndex = 11;
            this.kryptonButtonClear.Text = "Clear";
            this.kryptonButtonClear.Values.ExtraText = "";
            this.kryptonButtonClear.Values.Image = null;
            this.kryptonButtonClear.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonClear.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonClear.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonClear.Values.Text = "Clear";
            this.kryptonButtonClear.Click += new System.EventHandler(this.kryptonButtonClear_Click);
            // 
            // groupBoxSpectrumViewer
            // 
            this.groupBoxSpectrumViewer.Controls.Add(this.elementHost2);
            this.groupBoxSpectrumViewer.Controls.Add(this.elementHost1);
            this.groupBoxSpectrumViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSpectrumViewer.Location = new System.Drawing.Point(0, 0);
            this.groupBoxSpectrumViewer.Name = "groupBoxSpectrumViewer";
            this.groupBoxSpectrumViewer.Size = new System.Drawing.Size(749, 292);
            this.groupBoxSpectrumViewer.TabIndex = 13;
            this.groupBoxSpectrumViewer.TabStop = false;
            this.groupBoxSpectrumViewer.Text = "FIltered spectrum viewer : LeftClick + drag to zoom / Right Click to return";
            // 
            // elementHost2
            // 
            this.elementHost2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost2.Location = new System.Drawing.Point(3, 16);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(743, 273);
            this.elementHost2.TabIndex = 13;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.msViewer2;
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(3, 16);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(743, 273);
            this.elementHost1.TabIndex = 12;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.kryptonRichTextBoxIn);
            this.groupBox1.Controls.Add(this.kryptonButtonConvert);
            this.groupBox1.Controls.Add(this.kryptonRichTextBoxOut);
            this.groupBox1.Controls.Add(this.kryptonButtonClear);
            this.groupBox1.Controls.Add(this.kryptonLabel3);
            this.groupBox1.Controls.Add(this.kryptonLabelTotalPeaksRight);
            this.groupBox1.Controls.Add(this.kryptonLabelTotalPeaksLeft);
            this.groupBox1.Controls.Add(this.kryptonLabelTotalPeaks);
            this.groupBox1.Location = new System.Drawing.Point(215, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(531, 129);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Paste input spectrum in the left box, click arrow to filter";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.Size = new System.Drawing.Size(749, 31);
            this.kryptonHeader1.TabIndex = 16;
            this.kryptonHeader1.Text = "FT Filter Toy";
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "FT Filter Toy";
            this.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 31);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.kryptonLabel1);
            this.splitContainer1.Panel1.Controls.Add(this.kryptonNumericUpDownMinMSIntensity);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.kryptonNumericUpDownPPM);
            this.splitContainer1.Panel1.Controls.Add(this.kryptonLabel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxSpectrumViewer);
            this.splitContainer1.Size = new System.Drawing.Size(749, 446);
            this.splitContainer1.SplitterDistance = 150;
            this.splitContainer1.TabIndex = 17;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.kryptonLabel4);
            this.groupBox2.Controls.Add(this.kryptonCheckBoxGaussianFilter);
            this.groupBox2.Controls.Add(this.kryptonLabelGFCutoff);
            this.groupBox2.Location = new System.Drawing.Point(3, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 73);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Gaussian EM";
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(7, 45);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel4.TabIndex = 19;
            this.kryptonLabel4.Text = "Cutoff:";
            this.kryptonLabel4.Values.ExtraText = "";
            this.kryptonLabel4.Values.Image = null;
            this.kryptonLabel4.Values.Text = "Cutoff:";
            // 
            // kryptonCheckBoxGaussianFilter
            // 
            this.kryptonCheckBoxGaussianFilter.Location = new System.Drawing.Point(6, 19);
            this.kryptonCheckBoxGaussianFilter.Name = "kryptonCheckBoxGaussianFilter";
            this.kryptonCheckBoxGaussianFilter.Size = new System.Drawing.Size(93, 20);
            this.kryptonCheckBoxGaussianFilter.TabIndex = 16;
            this.kryptonCheckBoxGaussianFilter.Text = "Gaussian Fltr";
            this.kryptonCheckBoxGaussianFilter.Values.ExtraText = "";
            this.kryptonCheckBoxGaussianFilter.Values.Image = null;
            this.kryptonCheckBoxGaussianFilter.Values.Text = "Gaussian Fltr";
            // 
            // kryptonLabelGFCutoff
            // 
            this.kryptonLabelGFCutoff.Location = new System.Drawing.Point(54, 45);
            this.kryptonLabelGFCutoff.Name = "kryptonLabelGFCutoff";
            this.kryptonLabelGFCutoff.Size = new System.Drawing.Size(16, 20);
            this.kryptonLabelGFCutoff.TabIndex = 17;
            this.kryptonLabelGFCutoff.Text = "?";
            this.kryptonLabelGFCutoff.Values.ExtraText = "";
            this.kryptonLabelGFCutoff.Values.Image = null;
            this.kryptonLabelGFCutoff.Values.Text = "?";
            // 
            // SpectrumFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.kryptonHeader1);
            this.Name = "SpectrumFilter";
            this.Size = new System.Drawing.Size(749, 477);
            this.groupBoxSpectrumViewer.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonRichTextBox kryptonRichTextBoxIn;
        private ComponentFactory.Krypton.Toolkit.KryptonRichTextBox kryptonRichTextBoxOut;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonConvert;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMinMSIntensity;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownPPM;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabelTotalPeaksLeft;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabelTotalPeaks;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabelTotalPeaksRight;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonClear;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.GroupBox groupBoxSpectrumViewer;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private PatternTools.SpectrumViewer.MSViewer msViewer2;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxGaussianFilter;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabelGFCutoff;
        private System.Windows.Forms.GroupBox groupBox2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
