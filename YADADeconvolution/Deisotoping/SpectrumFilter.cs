﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PatternTools.GaussianMixture;

namespace PatternTools.MSParser.Deisotoping
{
    public partial class SpectrumFilter : UserControl
    {
        public SpectrumFilter()
        {
            InitializeComponent();
        }

        private void kryptonButtonConvert_Click(object sender, EventArgs e)
        {
            try
            {
                kryptonRichTextBoxOut.Clear();
                kryptonButtonConvert.Text = "Working!";
                this.Update();
                //Parse input

                double intensityThreshold = 0;

                MSFull myMS = DeisotopeTools.RichTextBox2MS(kryptonRichTextBoxIn.Text, 0);
                kryptonLabelTotalPeaksLeft.Text = myMS.MSData.Count.ToString();

                //Apply monotonically increasing peaks filter
                DeisotopeTools.RetainOnlyLocalMaximumPeaks(myMS, (double)kryptonNumericUpDownPPM.Value);

                foreach (Ion i in myMS.MSData)
                {
                    kryptonRichTextBoxOut.AppendText(i.MZ + "\t" + i.Intensity + "\n");
                }
                

                if (kryptonCheckBoxGaussianFilter.Checked)
                {
                    //Lets kick in the Gaussian Filter
                    //Extract the intensities
                    GaussianMixtureEM.Mixture GaussianMixture = new PatternTools.GaussianMixtureEM.Mixture(myMS.MSData, true);
                    intensityThreshold = GaussianMixture.PredictOptimalBayesianClassifier();
                    kryptonLabelGFCutoff.Text = intensityThreshold.ToString();

                }

                //Remove peaks below the cutoff
                if (intensityThreshold < (double)kryptonNumericUpDownMinMSIntensity.Value)
                {
                    intensityThreshold = (double)kryptonNumericUpDownMinMSIntensity.Value;
                }

                myMS.MSData.RemoveAll(p => p.Intensity < intensityThreshold);

                

                kryptonLabelTotalPeaksRight.Text = myMS.MSData.Count.ToString();

                msViewer2.MyMS = myMS;
                msViewer2.FuncPrintMS();
                
            }
            catch (Exception e2)
            {
                ErrorWindow ew = new ErrorWindow(e2.Message);
                ew.ShowDialog();
            }

            kryptonButtonConvert.Text = "->";
        }

        private void kryptonButtonClear_Click(object sender, EventArgs e)
        {
            kryptonRichTextBoxIn.Clear();
            kryptonRichTextBoxOut.Clear();
        }
        
    }
}
