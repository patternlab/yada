﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PatternTools.MSParser.Deisotoping
{
    public class IsotopicKNProfileGenerator
    {
        List<PatternTools.KernelRegressor> regressors;
        int signalCounter;
        double lastSignal;

        public IsotopicKNProfileGenerator() {
            
            regressors = new List<KernelRegressor> { 
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak0), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak1), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak2), 1650),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak3), 1650),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak4), 1650),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak5), 1650),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak6), 1650),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak7), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak8), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak9), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak10), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak11), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak12), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak13), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak14), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak15), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak16), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak17), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak18), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak19), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak20), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak21), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak22), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak23), 1700),
                new KernelRegressor(GeneratePointArray(Properties.Resources.peak24), 1700),
            };
        }

        private List<KernelRegressor.Point> GeneratePointArray(string p)
        {
            List<KernelRegressor.Point> result = new List<KernelRegressor.Point>();

            string[] lines = Regex.Split(p, "\n");

            foreach (string line in lines)
            {
                string [] cols = Regex.Split(line, "\t");
                result.Add(new KernelRegressor.Point(double.Parse(cols[0]), double.Parse(cols[1]), double.Parse(cols[2])));
            }

            return result;
        }


        //-----------------------------------------------

        


        public List<double> GetSignal(int numPeaks, double mass, double minIntensity)
        {

            List<double> result = new List<double>(numPeaks);
            if (numPeaks > 25)
            {
                throw (new Exception("Maximum of nine peaks"));
            }

            if (mass > 30000)
            {
                //Console.WriteLine("Isotopic signal generator message:");
                //Console.WriteLine("Real mass: " + mass + " Predicted signal for 21000");
                //throw (new Exception("Maximum mass of 21000"));
                mass = 30000;
            }


            signalCounter = 0;
            lastSignal = 1;
            bool only0 = false;

            for (int i = 0; i < numPeaks; i++)
            {
                //First peak
                if (signalCounter > 2 && lastSignal == 0 || only0)
                {
                    result.Add(0);
                    only0 = true;
                } else if  (i == 0)
                {
                    if (mass < 1500)
                    {
                        result.Add(1);
                        signalCounter++;
                    }
                    else if (mass > 12000)
                    {
                        result.Add(0);
                    }
                    else
                    {
                        result.Add(CorrectKernel(regressors[i].KernelCompute(mass), minIntensity, ref signalCounter));
                    }
                }
                else if (i == 1)
                {
                    //Second peak
                    if (mass < 500)
                    {
                        result.Add(0.25);
                        signalCounter++;
                    }
                    else
                    {
                        result.Add(CorrectKernel(regressors[i].KernelCompute(mass), minIntensity, ref signalCounter));
                    }
                }
                else if (i == 2)
                {
                    //Third Peak
                    if (mass < 500)
                    {
                        result.Add(0.05);
                        signalCounter++;
                    }
                    else
                    {
                        result.Add(CorrectKernel(regressors[i].KernelCompute(mass), minIntensity, ref signalCounter));
                    }
                } else if (i ==4 && mass > 23000) {
                    
                    result.Add(0);

                }else {

                    result.Add(CorrectKernel(regressors[i].KernelCompute(mass), minIntensity, ref signalCounter));
                }

                lastSignal = result[result.Count - 1];

            }
            return result;

        }

        private double CorrectKernel(double kernelOutput, double minIntensity, ref int signalCounter)
        {
            if (kernelOutput > 1)
            {
                signalCounter++;
                return (1);
            }
            else if (kernelOutput < minIntensity)
            {
                return 0;
            }
            else
            {
                signalCounter++;
                return (kernelOutput);
            }
        }

    }
}
