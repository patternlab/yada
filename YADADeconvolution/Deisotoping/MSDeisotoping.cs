﻿using System;
using System.Collections.Generic;
using System.Text;
using PatternTools.MSParser.Deisotoping;
using System.Text.RegularExpressions;
using System.Linq;

namespace PatternTools.MSParser
{
    public class MSDeisotoper
    {

        //Isotopic Steps High
        double ppm;
        double isotopicDistributionRelativeCutoff = 0.01;
        
        int maxChargeState;
        int numberOfPeaksToConsider = 24;
        Regex regexIsITMS = new Regex(@"ITMS", RegexOptions.Compiled);

        //IsotopicKNProfileGenerator envelopeSignal = new IsotopicKNProfileGenerator();
        IsotopicSplineSignalGenerator envelopeSignal = new IsotopicSplineSignalGenerator();

        IsotopicSignalGenerator signalGenerator;


        //Always enter something conservative for ppm deisotopng (at least 1.5X)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ppm"></param>
        /// <param name="maxChargeState">The max charge state must be between 7 and 18</param>
        public MSDeisotoper(double ppm, int maxChargeState)
        {

            //if (maxChargeState > 18) { throw new Exception("The max charge state must be between 7 and 18"); }
            this.ppm = ppm;
            this.maxChargeState = maxChargeState;

            if (maxChargeState >= 16)
            {
                numberOfPeaksToConsider = 24;
            }
            else if (maxChargeState > 12)
            {
                numberOfPeaksToConsider = 15;
            }
            else
            {
                numberOfPeaksToConsider = 12;
            }

             signalGenerator = new IsotopicSignalGenerator(maxChargeState, numberOfPeaksToConsider);

        }


        /// <summary>
        /// This method return a scoring matrix where the first column lists the index of the ion in the MSParser.MS, the second indicates a charge score
        /// Method will reject ITMS mass spectra
        /// </summary>
        /// <param name="theMS"></param>
        /// <param name="convolutedClusteringFacotor"></param>
        /// <param name="stringency"></param>
        /// <param name="skipChrg1"></param>
        /// <param name="maxNoPeaks"></param>
        /// <param name="minSequentialScore">For this parameter we suggest 2 for MS2 and 3 for MS1</param>
        /// <returns></returns>
        public List<EnvelopeScore> DeIsotopeMS(
            MSParser.MSFull theMS,
            double convolutedClusteringFacotor,
            double stringency,
            bool skipChrg1,
            int maxNoPeaks,
            bool correctForUndetectedIsotopes,
            bool skipChargePlusOneInMS2,
            bool skipChargePlusOneInMS1 = false)      
        {


            //We will not deisotope ITMS
            if ( !string.IsNullOrEmpty(theMS.InstrumentType))
            {
                if (regexIsITMS.IsMatch(theMS.InstrumentType))
                {
                    return new List<EnvelopeScore>();
                }
            }


            //lets use a normalized cross correlation to find the position of the +1, +2, +3, so on and so forth
            List<EnvelopeScore> envelopeScoreListTmp = new List<EnvelopeScore>(theMS.MSData.Count);

            for (int m = 0; m < theMS.MSData.Count; m++)
            {
                EnvelopeScore eScore = new EnvelopeScore();
                eScore.MZ = theMS.MSData[m].MZ;
                eScore.IntensityOfFirstPeak = theMS.MSData[m].Intensity;

                //Generate all possible envelope signals distributions before hand
                List<List<double>> envelopeProfiles = new List<List<double>>(signalGenerator.IsotopicSteps.Count);
                List<int> noItemsInProfile = new List<int>();
                for (int z = 0; z < signalGenerator.IsotopicSteps.Count; z++)
                {
                    double aproximateEstimatedMass = (double)(z + 1) * theMS.MSData[m].MZ;
                    List<double> profile = envelopeSignal.GetSignal(numberOfPeaksToConsider, aproximateEstimatedMass, isotopicDistributionRelativeCutoff);
                    List<double> r = profile.FindAll((a) => a> 0);
                    envelopeProfiles.Add(profile);
                    noItemsInProfile.Add(r.Count);
                }


                //acumulate signals
                List<List<double>> acumulatedSignals = new List<List<double>>(signalGenerator.IsotopicSteps.Count);

                for (int i = 0; i < signalGenerator.IsotopicSteps.Count; i++)
                {
                    //Find out how far we should acumulate
                    double delta = (noItemsInProfile[i] * (1.0024 / (double)(i+1))) + 0.1;
                    List<double> acumulatedSignal = getAcumulatedSignal(signalGenerator.IsotopicSteps[i].Steps, theMS, delta, m);
                    
                    //Correct accumulated signal
                    for (int j = noItemsInProfile[i]; j < acumulatedSignal.Count; j++)
                    {
                        acumulatedSignal[j] = 0;
                    }

                    //Add acumulated signal
                    acumulatedSignals.Add(acumulatedSignal);
                }


                //Lets store the ion intensity by summing the isotopic peaks
                List<double> acumulatedUnnormalizedIntensities = new List<double>(acumulatedSignals.Count);
                for (int b = 0; b < acumulatedSignals.Count; b++)
                {
                    acumulatedUnnormalizedIntensities.Add(acumulatedSignals[b].Sum());
                }


                //Perform the dot products
                for (int z = 0; z < acumulatedSignals.Count; z++)
                {
                    
                    acumulatedSignals[z] = pTools.UnitVector(acumulatedSignals[z]);

                    //Carry out the multiplication
                    List<double> theoreticalEnvelopeDistribution = PatternTools.pTools.UnitVector(envelopeProfiles[z]);

                    ChargeScore csi = new ChargeScore();
                    csi.Charge = z + 1;

                    csi.DotProductScore = PatternTools.pTools.DotProduct(theoreticalEnvelopeDistribution, acumulatedSignals[z]);

                    if (double.IsNaN(csi.DotProductScore))
                    {
                        throw (new Exception("NaN found"));
                    }

                    

                    csi.AcumulatedNormalizedSignal = acumulatedSignals[z];
                    csi.IntensityOfDeconvolutedMZ = acumulatedUnnormalizedIntensities[z];                   
                    eScore.ChargeScoreList.Add(csi);


                }



                envelopeScoreListTmp.Add(eScore);
            }


            //Eliminate all elements that have scores lower than the threshold
            List<EnvelopeScore> envelopeScoreList = new List<EnvelopeScore>();

            //Stage 3 PredictCharges and eliminate charges below certain threshold

            //Every time we eliminate someone, lets raise our stringency


            
            foreach (EnvelopeScore es in envelopeScoreListTmp)
            {

                //Strategy 2, sort by highest number of nonempty peaks
                foreach (var thisEn in es.ChargeScoreList)
                {
                    thisEn.SequentialPeakScore = 0;

                    for (int i = 0; i < thisEn.AcumulatedNormalizedSignal.Count; i++)
                    {
                        if (thisEn.AcumulatedNormalizedSignal[i] > 0)
                        {
                            thisEn.SequentialPeakScore++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                //We need to sort by peak score
                es.ChargeScoreList.Sort((a, b) => b.SequentialPeakScore.CompareTo(a.SequentialPeakScore));


                //Eliminate the ones that did not achieve the maximum peak score
                double maxSequentialPeakScore = es.ChargeScoreList[0].SequentialPeakScore;
                es.ChargeScoreList.RemoveAll(a => a.SequentialPeakScore < maxSequentialPeakScore || a.DotProductScore < stringency);

                if ((skipChrg1 && !theMS.isMS2) || (theMS.isMS2 && skipChargePlusOneInMS2))
                {
                    es.ChargeScoreList.RemoveAll(a => a.Charge == 1);
                }

                if (skipChargePlusOneInMS1 && !theMS.isMS2)
                {
                    es.ChargeScoreList.RemoveAll(a => a.Charge == 1);
                }

                if (es.ChargeScoreList.Count == 0) { continue; }



                //Sort by INTENSITY (works better than stringency
                es.ChargeScoreList.Sort((a, b) => b.IntensityOfDeconvolutedMZ.CompareTo(a.IntensityOfDeconvolutedMZ));
                

                //Now we need to see if it has the minimum number of peaks
                double aproxMass = es.ChargeScoreList[0].Charge * es.MZ;

                int minSequentialScore = 2;
                if (theMS.isMS2)
                {
                    minSequentialScore = 3;
                }

                if (aproxMass > 2000 || es.ChargeScoreList[0].Charge > 3)
                {
                    minSequentialScore = 3;
                }
                if (es.ChargeScoreList[0].Charge >= 8)
                {
                    minSequentialScore = 4;
                }

                //////////////////////////////////////////

                if (es.ChargeScoreList[0].SequentialPeakScore < minSequentialScore)
                {
                    continue;
                }

                
                
                //Store some properties
                es.PredictedCharge = es.ChargeScoreList[0].Charge;
                es.PredictedChargeScore = es.ChargeScoreList[0].DotProductScore;
                es.SummedIonIntensityForPredictedCharge = es.ChargeScoreList[0].IntensityOfDeconvolutedMZ;

                envelopeScoreList.Add(es);


            }



            //Post-process the list
            List<EnvelopeScore> filtered = new List<EnvelopeScore>();
            if (envelopeScoreList.Count == 0)
            {
                Console.Write("spectra skipped (0 isotopic envelopes) \n");
                return (new List<EnvelopeScore>()); 
            }


            ClusterEnvelope(convolutedClusteringFacotor, envelopeScoreList);

            filtered.Add(envelopeScoreList[0]);

            for (int i = 1; i < envelopeScoreList.Count; i++)
            {
                EnvelopeScore thisEnv = envelopeScoreList[i];

                if (thisEnv.MZ == 437.734072)
                {
                    Console.WriteLine(".");
                }


                EnvelopeScore last = filtered[filtered.Count-1];
                EnvelopeScore pres = envelopeScoreList[i];


                //Check if this is an isotopic peak
                bool isIsotopic = VerifyIsotopic(filtered, pres);
                if (isIsotopic) {
                    continue;
                }

                //If it got till here, it deserves to be added
                filtered.Add(thisEnv);                

            }


            //And make sure that we are not providing an envelope list larger than the upper bound
            if (filtered.Count > maxNoPeaks)
            {
                filtered.Sort((a, b) => b.SummedIonIntensityForPredictedCharge.CompareTo(a.SummedIonIntensityForPredictedCharge));
                filtered.RemoveRange(maxNoPeaks - 1, filtered.Count - maxNoPeaks);
            }


            //Lets give the result as a nice and sorted list
            filtered.Sort((a, b) => a.MZ.CompareTo(b.MZ));

            //Lets make sure we are not missing any undetected peaks for masses above 12 kDa
            if (correctForUndetectedIsotopes)
            {
                CorrectForUndetectedMonoIsotopes(12000, filtered);
            }

            return (filtered);

        }


        /// <summary>
        /// This strategy will alter the MZ of the detected envelope to a theoretically predicted one based on the 3 most intense peaks of the envelope
        /// </summary>
        /// <param name="minimumMass">The minimum mass that triggers this feature</param>
        /// <param name="envelopes">The envelope list we wish to search for undetected monoisotopic peaks</param>
        private void CorrectForUndetectedMonoIsotopes(double minimumMass, List<EnvelopeScore> envelopes)
        {
            foreach (EnvelopeScore envelope in envelopes)
            {
                double approxMass = envelope.MZ * envelope.PredictedCharge;

                if (approxMass > minimumMass && envelope.ChargeScoreList[0].SequentialPeakScore > 5 && envelope.ChargeScoreList[0].DotProductScore > 0.9)
                {
                    //Obtain a full averagine model
                    List<double> theoreticalProfile = envelopeSignal.GetSignal(20, approxMass, 0);

                    //Find the starting position for tripplet that yields the greatest signal in the theoretical envelope
                    double theoreticalDistanceFromMostIntenseTripplet = FindDistanceOfIsotopicPeakFromMostIntenseCouple(theoreticalProfile);

                    //Now find the distance from our empirically obtained signal
                    List<double> detectedPeaks = envelope.ChargeScoreList[0].AcumulatedNormalizedSignal;
                    double empriricalDistanceFromMostIntenseTripplet = FindDistanceOfIsotopicPeakFromMostIntenseCouple(detectedPeaks);

                    if (theoreticalDistanceFromMostIntenseTripplet > empriricalDistanceFromMostIntenseTripplet)
                    {
                        double theDifferenceInPeaks = theoreticalDistanceFromMostIntenseTripplet - empriricalDistanceFromMostIntenseTripplet;

                        //Lets top it to two for safety reasons
                        if (theDifferenceInPeaks > 2) {
                            theDifferenceInPeaks = 2;
                        }

                        double mz2Subtract = (theDifferenceInPeaks) * IsotopicSignalGenerator.DeltaCarbon13 / envelope.PredictedCharge;
                        envelope.MZ -= mz2Subtract;
                    }
                }
            }
        }

        private static double FindDistanceOfIsotopicPeakFromMostIntenseCouple(List<double> theoreticalProfile)
        {
            double distanceFromMostIntenseTripplet = 0;
            double maxSignal = double.MinValue;
            for (int i = 0; i < theoreticalProfile.Count - 1; i++)
            {
                double signal = theoreticalProfile[i] + theoreticalProfile[i+1];

                if (signal > maxSignal)
                {
                    maxSignal = signal;
                    distanceFromMostIntenseTripplet = i;
                }
            }

            return (distanceFromMostIntenseTripplet);
        }


        //--------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Merges peaks that are very close, works like a centroid algorithm
        /// </summary>
        /// <param name="deconvolutedClusteringFactor"></param>
        /// <param name="theMS"></param>
        public void ClusterMSPeaks(double deconvolutedClusteringFactor, MSFull theMS)
        {
            bool needsToMerge = true;
            int minStepsAppart = 4;
            int stepCounter = 0;
            List<Pairs> thePairs = new List<Pairs>();
            theMS.MSData.Sort((a, b) => a.MZ.CompareTo(b.MZ));

            while (needsToMerge)
            {
                needsToMerge = false;
                thePairs.Clear();
                //Search for the index to merge
                for (int i = 1; i < theMS.MSData.Count; i++)
                {
                    stepCounter++;
                    double difference = theMS.MSData[i].MZ - theMS.MSData[i - 1].MZ;

                    if (difference < deconvolutedClusteringFactor && stepCounter > minStepsAppart)
                    {
                        needsToMerge = true;
                        stepCounter = 0;

                        Pairs p = new Pairs();
                        p.Ion1 = theMS.MSData[i];
                        p.Ion2 = theMS.MSData[i - 1];
                        thePairs.Add(p);
                    }
                }

                if (needsToMerge)
                {
                    foreach (Pairs p in thePairs)
                    {
                        p.Ion1.MZ = (p.Ion1.MZ + p.Ion2.MZ) / 2;

                        //Remove the merged 1
                        theMS.MSData.Remove(p.Ion2);
                    }
                }

            }

        }
        /// <summary>
        /// Used for the clustering method
        /// </summary>
        struct Pairs
        {
            public Ion Ion1 { get; set; }
            public Ion Ion2 { get; set; }
        }

        //Used for envelope clustering
        struct EnvelopePairs
        {
            public EnvelopeScore e1 { get; set; }
            public EnvelopeScore e2 { get; set; }
        }

        public static void ClusterEnvelope(double convolutedClusteringFactor, List<EnvelopeScore> envelopeScoreList)
        {
            bool needsToMerge = true;
            List<EnvelopePairs> envelopePairs = new List<EnvelopePairs>();

            while (needsToMerge)
            {

                needsToMerge = false;
                envelopePairs.Clear();

                //Search for the index to merge
                for (int i = 1; i < envelopeScoreList.Count; i++)
                {
                    double difference = envelopeScoreList[i].MZ - envelopeScoreList[i - 1].MZ;

                    if (difference < convolutedClusteringFactor)
                    {
                        EnvelopePairs ep = new EnvelopePairs();
                        ep.e1 = envelopeScoreList[i];
                        ep.e2 = envelopeScoreList[i - 1];

                        if (ep.e1.PredictedCharge != ep.e2.PredictedCharge)
                        {
                            continue;
                        }
                        envelopePairs.Add(ep);
                        needsToMerge = true;
                    }
                }

                if (needsToMerge)
                {
                    //merge
                    foreach (EnvelopePairs ep in envelopePairs)
                    {
                        //if (ep.e1.MZ == 1034.954884 || ep.e2.MZ == 1034.954884)
                        //{
                        //    Console.WriteLine("Debug");
                        //}

                        if (ep.e1.SummedIonIntensityForPredictedCharge < ep.e2.SummedIonIntensityForPredictedCharge)
                        {
                            envelopeScoreList.Remove(ep.e1);
                        }
                        else
                        {
                            envelopeScoreList.Remove(ep.e2);
                        }
                        
                    }
                }

            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxDifference"></param>
        /// <param name="theMS"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        private List<double> getAcumulatedSignal(List<double> isotopicSteps, MSFull theMS, double maxDifference, int StartArrayPosition)
        {
            double difference = 0;
            int stepAhead = StartArrayPosition;

            List<double> acumulatedSignal = new List<double>(new double[isotopicSteps.Count]);

            while (true)
            {
                if (stepAhead >= theMS.MSData.Count)
                {
                    break;
                }


                for (int isostep = 0; isostep < isotopicSteps.Count; isostep++)
                {
                    double comparisonMass = theMS.MSData[StartArrayPosition].MZ + isotopicSteps[isostep];
                    double theppm = PatternTools.pTools.PPM(theMS.MSData[stepAhead].MZ, comparisonMass);
                    double delta = theMS.MSData[stepAhead].MZ - theMS.MSData[StartArrayPosition].MZ; // For tracking purposes
                   
                    if (theppm <= ppm)
                    {
                        acumulatedSignal[isostep] += theMS.MSData[stepAhead].Intensity;
                        
                        break;
                    }
                    else if (comparisonMass > theMS.MSData[stepAhead].MZ + 1)
                    {
                        break;
                    }
                }

                difference = theMS.MSData[stepAhead].MZ - theMS.MSData[StartArrayPosition].MZ;
                stepAhead++;

                if (difference > maxDifference)
                {
                    break;
                }

            }

            if (double.IsNaN(acumulatedSignal[0])) {
                throw new Exception ("Problems in the signal acumulation algorithm");
            }

            return acumulatedSignal;

        }

        private bool VerifyIsotopic(List<EnvelopeScore> filtered, EnvelopeScore pres)
        {
            bool isotopic = false;
            double aproximateMass = pres.MZ * pres.PredictedCharge;


            foreach (EnvelopeScore s in filtered)
            {

                double diff = pres.MZ - s.MZ;



                if (diff > 2.5 || diff < 0) { continue; }

                //if (pres.MZ == 971.379284)
                //{
                //    Console.WriteLine("");
                //}


                //To eliminate things such as charge 2 and 4 overlap
                double modulous = (double)pres.PredictedCharge % (double)s.PredictedCharge;
                if (modulous > 0 && pres.PredictedCharge <= s.PredictedCharge)
                {
                    continue;
                }

                int noOfIsoPeaksToConsider = (int)s.ChargeScoreList[0].SequentialPeakScore;

                if (s.PredictedCharge >= pres.PredictedCharge)
                {

                    for (double i = 1; i <= noOfIsoPeaksToConsider + 1; i++)
                    {
                        double goal = s.MZ + ((i + i * 0.0027) / pres.PredictedCharge);
                        double ppmDif = PatternTools.pTools.PPM(pres.MZ, goal);
                        if (ppmDif < ppm)
                        {
                            return (true);
                        }
                    }
                }

            }
            return isotopic;
        }

    }
}
