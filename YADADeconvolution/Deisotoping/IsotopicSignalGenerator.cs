﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatternTools.MSParser.Deisotoping
{

    public class IsotopicSignalGenerator
    {
        List<IsotopicStep> isotopicSteps = new List<IsotopicStep>();
        //public const double DeltaCarbon13 = 1.003354826;
        public const double DeltaCarbon13 = 1.0022; //Corrected

        public List<IsotopicStep> IsotopicSteps { get { return isotopicSteps; } }


        public IsotopicSignalGenerator(double isotopicStepsMaxCharge, int noIsotopicSteps)
        {
            //Generate the isotopic steps
            isotopicSteps = new List<IsotopicStep>((int)isotopicStepsMaxCharge);

            for (int i = 0; i < isotopicStepsMaxCharge; i++)
            {
                isotopicSteps.Add(GenerateIsotopicSteps((double)(i + 1), noIsotopicSteps));
            }
        }



        public static IsotopicStep GenerateIsotopicSteps(double charge, int maxSteps)
        {

            List<IsotopicStep> result = new List<IsotopicStep>();
            
            IsotopicStep iStep = new IsotopicStep();
            iStep.Steps = new List<double>(maxSteps);
            iStep.Charge = charge;

            for (double s = 0; s < maxSteps; s++)
            {
                double stepValue = (s * DeltaCarbon13) / charge;
                iStep.Steps.Add(stepValue);
            }

            return iStep;
        }
    }

    public class IsotopicStep
    {
        public double Charge { get; set; }
        public List<double> Steps { get; set; }
    }
}
