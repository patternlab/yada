﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using PatternTools.MSParser;
using System.Text.RegularExpressions;
using PatternTools.GaussianMixture;

namespace PatternTools.MSParser.Deisotoping
{
    public partial class IsotopicFinder : UserControl
    {
        public IsotopicFinder()
        {
            InitializeComponent();
        }

        private void kryptonButtonClearSpectrum_Click(object sender, EventArgs e)
        {
            kryptonRichTextBoxSpectrum.Clear();
        }

        private void kryptonButtonWork_Click(object sender, EventArgs e)
        {

            kryptonButtonWork.Text = "Working!"; this.Update();

            richTextBoxResults.Clear();

            //parse spectrum to a MS object
            Regex lineSplitter = new Regex(@"\n");
            Regex spaceSplitter = new Regex(@"[ |\t]");

            PatternTools.MSParser.MSFull myMS = DeisotopeTools.RichTextBox2MS(kryptonRichTextBoxSpectrum.Text, (double)kryptonNumericUpDownCutoff.Value);

            try
            {
                if (kryptonCheckBoxGaussianFilter.Checked)
                {
                    GaussianMixtureEM.Mixture mix = new PatternTools.GaussianMixtureEM.Mixture(myMS.MSData, true);
                    double intensityThreshold = mix.PredictOptimalBayesianClassifier();
                    myMS.MSData.RemoveAll(a => a.Intensity < intensityThreshold);
                }
            }
            catch (Exception e2)
            {
                ErrorWindow ew = new ErrorWindow("Gaussian Filter will not used:" + e2.Message);
                ew.ShowDialog();
            }

            //Just to make sure
            if (myMS.MSData.Count == 0)
            {
                richTextBoxResults.Clear();
                richTextBoxResults.AppendText("0 ions above min intensity in input box");
                kryptonButtonWork.Text = "Go!";
                return;
            }

            if (kryptonCheckBoxRetainOnlyLocalMaximumPeaks.Checked)
            {
                DeisotopeTools.RetainOnlyLocalMaximumPeaks(myMS, (double)kryptonNumericUpDownFTFilteringPPM.Value);
            }

            if (kryptonCheckBoxIsMS2.Checked)
            {
                myMS.isMS2 = true;
            }


            double[] chargeCounter = new double[10];
            int counter = 0;

            PatternTools.MSParser.MSDeisotoper deIsotoper = new MSDeisotoper((double)kryptonNumericUpDownPPM.Value, (int)kryptonNumericUpDownMaxCharge.Value);
            List<PatternTools.MSParser.EnvelopeScore> envelopeScoreList;
            envelopeScoreList = deIsotoper.DeIsotopeMS(myMS,
                (double)kryptonNumericUpDownCLusteringTolerance.Value,
                (double)kryptonNumericUpDownStringency.Value,
                kryptonCheckBoxSkipChrg1.Checked,
                (int)kryptonNumericUpDownMaxNoPeaks.Value,
                kryptonCheckBoxUndetectedPrecursors.Checked,
                false,
                false
                );


            foreach (EnvelopeScore es in envelopeScoreList)
            {

                counter++;
                richTextBoxResults.AppendText(counter + " : " + es.MZ + "\n");

                //print the scores

                for (int i = 0; i < es.ChargeScoreList.Count; i++)
                {
                    richTextBoxResults.AppendText("  :Score: " + Math.Round(es.ChargeScoreList[i].DotProductScore, 3) + ", Z: " + es.ChargeScoreList[i].Charge + ", Summed Intensity: " + Math.Round(es.ChargeScoreList[i].IntensityOfDeconvolutedMZ, 1) + ", MonoIso: " + Math.Round(PatternTools.pTools.DechargeMSPeakToPlus1(es.MZ, es.ChargeScoreList[i].Charge), 2) + ", SequentialScore: " + es.ChargeScoreList[i].SequentialPeakScore + "\n");
                }

                richTextBoxResults.AppendText("\n");

                //Update the GUI
                try
                {
                    chargeCounter[(int)(es.PredictedCharge - 1)]++;
                } catch  {
                    //Actualy, nothing is wrong; it is just that we will not print it to screen.
                }

            }

            //Fill out the deconvoluted spectrum box
            richTextBoxDeconvolutedSpectrum.Clear();
            List<Ion> deconvolutedIons = new List<Ion>();
            foreach (EnvelopeScore es in envelopeScoreList)
            {
                Ion i = new Ion(PatternTools.pTools.DechargeMSPeakToPlus1(es.MZ, es.PredictedCharge), es.SummedIonIntensityForPredictedCharge, 0, 0);
                deconvolutedIons.Add(i);
            }
            deconvolutedIons.Sort((a,b) => a.MZ.CompareTo(b.MZ));

            PatternTools.MSParser.MSFull ms = new MSFull();
            ms.MSData = deconvolutedIons;
            ms.ClusterMSIons(0.02);

            foreach (Ion i in ms.MSData) {
                richTextBoxDeconvolutedSpectrum.AppendText(Math.Round(i.MZ,4) + " " + Math.Round(i.Intensity,1)+"\n");
            }


            //Update the labels
            kryptonLabelC1.Text = chargeCounter[0].ToString();
            kryptonLabelC2.Text = chargeCounter[1].ToString();
            kryptonLabelC3.Text = chargeCounter[2].ToString();
            kryptonLabelC4.Text = chargeCounter[3].ToString();
            kryptonLabelC5.Text = chargeCounter[4].ToString();
            kryptonLabelC6.Text = chargeCounter[5].ToString();
            kryptonLabelC7.Text = chargeCounter[6].ToString();
            kryptonLabelC8.Text = chargeCounter[8].ToString();
            kryptonLabelC9.Text = chargeCounter[9].ToString();

            kryptonLabelTotal.Text = counter.ToString();

            kryptonButtonWork.Text = "Go!";

        }

        private void kryptonCheckBoxGaussianFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (kryptonCheckBoxGaussianFilter.Checked)
            {
                kryptonNumericUpDownCutoff.Enabled = false;
            }
            else
            {
                kryptonNumericUpDownCutoff.Enabled = true;
            }
        }


        
    }

    
}
