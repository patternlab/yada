﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatternTools.MSParser.Deisotoping;
using PatternTools.MSParser;

namespace PatternTools.MSParser
{
    public class DeconvoluteSimple
    {
        MSDeisotoper deIsotoper;
        double ppm;

        public DeconvoluteSimple(double ppm, int maxCharge)
        {
            deIsotoper = new MSDeisotoper(ppm, maxCharge);
            this.ppm = ppm;
        }

        public List<EnvelopeScore> DeconvoluteMS(MSFull myMS, double convolutionClusteringFactor, double stringency, bool skipPlus1)
        {
            DeisotopeTools.RetainOnlyLocalMaximumPeaks(myMS, ppm);
            myMS.isMS2 = true;


            //MSParser.MSFull theMS,
            //double convolutedClusteringFacotor,
            //double stringency,
            //bool skipChrg1,
            //int maxNoPeaks,
            //bool correctForUndetectedIsotopes,
            //bool skipChargePlusOneInMS2

            List<EnvelopeScore> envelopeScoreList = deIsotoper.DeIsotopeMS(
                myMS,
                convolutionClusteringFactor,
                stringency,
                skipPlus1,
                300,
                false,
                false,
                false
                );

            return envelopeScoreList;

        }

    }
}