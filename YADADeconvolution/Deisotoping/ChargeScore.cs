﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatternTools.MSParser
{
    public class ChargeScore
    {
        public double Charge { get; set; }
        public double DotProductScore { get; set; }
        public double SequentialPeakScore { get; set; }
        public List<double> AcumulatedNormalizedSignal { get; set; }
        public double IntensityOfDeconvolutedMZ { get; set; }
    }
}
