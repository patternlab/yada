﻿namespace PatternTools.MSParser.Deisotoping
{
    partial class EnvelopeIntensityGenerator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EnvelopeIntensityGenerator));
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonButtonGO = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonRichTextBoxResult = new ComponentFactory.Krypton.Toolkit.KryptonRichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownMass = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonNumericUpDownSignalCutoff = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(17, 37);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(493, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Text = "Uses a spline 3 regressor to generate the signal, therefore the result is an appr" +
                "oximation.";
            this.kryptonLabel1.Values.ExtraText = "";
            this.kryptonLabel1.Values.Image = null;
            this.kryptonLabel1.Values.Text = "Uses a spline 3 regressor to generate the signal, therefore the result is an appr" +
                "oximation.";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.Size = new System.Drawing.Size(601, 31);
            this.kryptonHeader1.TabIndex = 2;
            this.kryptonHeader1.Text = "Isotopic aproximate signal generator";
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Isotopic aproximate signal generator";
            this.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(6, 24);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(41, 20);
            this.kryptonLabel2.TabIndex = 3;
            this.kryptonLabel2.Text = "Mass: ";
            this.kryptonLabel2.Values.ExtraText = "";
            this.kryptonLabel2.Values.Image = null;
            this.kryptonLabel2.Values.Text = "Mass: ";
            // 
            // kryptonButtonGO
            // 
            this.kryptonButtonGO.Location = new System.Drawing.Point(6, 78);
            this.kryptonButtonGO.Name = "kryptonButtonGO";
            this.kryptonButtonGO.Size = new System.Drawing.Size(153, 25);
            this.kryptonButtonGO.TabIndex = 5;
            this.kryptonButtonGO.Text = "GO";
            this.kryptonButtonGO.Values.ExtraText = "";
            this.kryptonButtonGO.Values.Image = null;
            this.kryptonButtonGO.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonGO.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonGO.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonGO.Values.Text = "GO";
            this.kryptonButtonGO.Click += new System.EventHandler(this.kryptonButtonGO_Click);
            // 
            // kryptonRichTextBoxResult
            // 
            this.kryptonRichTextBoxResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonRichTextBoxResult.Location = new System.Drawing.Point(3, 16);
            this.kryptonRichTextBoxResult.Name = "kryptonRichTextBoxResult";
            this.kryptonRichTextBoxResult.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Both;
            this.kryptonRichTextBoxResult.Size = new System.Drawing.Size(237, 201);
            this.kryptonRichTextBoxResult.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.kryptonRichTextBoxResult);
            this.groupBox1.Location = new System.Drawing.Point(203, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 220);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Result";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(17, 63);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(317, 20);
            this.kryptonLabel3.TabIndex = 8;
            this.kryptonLabel3.Text = "Works up to 30 kDa and reports up to the first 25 peaks. ";
            this.kryptonLabel3.Values.ExtraText = "";
            this.kryptonLabel3.Values.Image = null;
            this.kryptonLabel3.Values.Text = "Works up to 30 kDa and reports up to the first 25 peaks. ";
            // 
            // kryptonNumericUpDownMass
            // 
            this.kryptonNumericUpDownMass.Location = new System.Drawing.Point(53, 22);
            this.kryptonNumericUpDownMass.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMass.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.kryptonNumericUpDownMass.Name = "kryptonNumericUpDownMass";
            this.kryptonNumericUpDownMass.Size = new System.Drawing.Size(106, 22);
            this.kryptonNumericUpDownMass.TabIndex = 9;
            this.kryptonNumericUpDownMass.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // kryptonNumericUpDownSignalCutoff
            // 
            this.kryptonNumericUpDownSignalCutoff.DecimalPlaces = 2;
            this.kryptonNumericUpDownSignalCutoff.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.kryptonNumericUpDownSignalCutoff.Location = new System.Drawing.Point(95, 50);
            this.kryptonNumericUpDownSignalCutoff.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kryptonNumericUpDownSignalCutoff.Name = "kryptonNumericUpDownSignalCutoff";
            this.kryptonNumericUpDownSignalCutoff.Size = new System.Drawing.Size(64, 22);
            this.kryptonNumericUpDownSignalCutoff.TabIndex = 10;
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(6, 50);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(83, 20);
            this.kryptonLabel4.TabIndex = 11;
            this.kryptonLabel4.Text = "Signal Cutoff: ";
            this.kryptonLabel4.Values.ExtraText = "";
            this.kryptonLabel4.Values.Image = null;
            this.kryptonLabel4.Values.Text = "Signal Cutoff: ";
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.kryptonButtonGO);
            this.groupBoxSettings.Controls.Add(this.kryptonLabel4);
            this.groupBoxSettings.Controls.Add(this.kryptonLabel2);
            this.groupBoxSettings.Controls.Add(this.kryptonNumericUpDownSignalCutoff);
            this.groupBoxSettings.Controls.Add(this.kryptonNumericUpDownMass);
            this.groupBoxSettings.Location = new System.Drawing.Point(17, 103);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(180, 220);
            this.groupBoxSettings.TabIndex = 12;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Settings";
            // 
            // EnvelopeIntensityGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxSettings);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.kryptonHeader1);
            this.Controls.Add(this.kryptonLabel1);
            this.Name = "EnvelopeIntensityGenerator";
            this.Size = new System.Drawing.Size(601, 356);
            this.groupBox1.ResumeLayout(false);
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonGO;
        private ComponentFactory.Krypton.Toolkit.KryptonRichTextBox kryptonRichTextBoxResult;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMass;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownSignalCutoff;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private System.Windows.Forms.GroupBox groupBoxSettings;
    }
}
