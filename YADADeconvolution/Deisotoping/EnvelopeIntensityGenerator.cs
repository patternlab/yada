﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PatternTools.MSParser.Deisotoping
{
    public partial class EnvelopeIntensityGenerator : UserControl
    {
        //IsotopicKNProfileGenerator isg = new IsotopicKNProfileGenerator();
        IsotopicSplineSignalGenerator isg = new IsotopicSplineSignalGenerator();

        public EnvelopeIntensityGenerator()
        {
            InitializeComponent();
        }

        private void kryptonButtonGO_Click(object sender, EventArgs e)
        {
            kryptonRichTextBoxResult.Text = "";
            List<double> signal = isg.GetSignal(24, (double)kryptonNumericUpDownMass.Value, (double)kryptonNumericUpDownSignalCutoff.Value);

            for (int i = 0; i < signal.Count; i++)
            {
                kryptonRichTextBoxResult.AppendText((i).ToString() + "\t" + signal[i] + "\n");
            }
        }
    }
}
