﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatternTools.MSParser
{
    public class EnvelopeScore
    {
        public EnvelopeScore()
        {
            clusterBuffer = new List<double>();
        }

        public double MZ { get; set; }

        /// <summary>
        /// Used for clustering simmilar peaks
        /// </summary>
        public List<double> clusterBuffer { get; set; }


        /// <summary>
        /// The predicted charge is the hypothesis of highest charge that scores above a certain threshold
        /// </summary>
        public double PredictedCharge { get; set; }
        public double PredictedChargeScore { get; set; }
        public double SummedIonIntensityForPredictedCharge { get; set; }
        public double IntensityOfFirstPeak { get; set; }

        //--------------------------------
        List<ChargeScore> chargeScoreList = new List<ChargeScore>();

        public List<ChargeScore> ChargeScoreList
        {
            get { return chargeScoreList; }
            set { chargeScoreList = value; }
        }

        //---------------------------------

    }
}
