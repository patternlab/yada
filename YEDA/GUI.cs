﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using PatternTools.MSParser;
using PatternTools.MSParser.Deisotoping;
using PatternTools;
using PatternTools.MSChopper;
using System.Linq;
using PatternTools.RawReader;
using PatternTools.MSParserLight;
using RawReader;

namespace YADA
{
    
    public partial class GUI : Form
    {

        Decharger decharger = new Decharger();
        ExecutionParams eParams = new ExecutionParams();
        
        public GUI()
        {
            InitializeComponent();
            
            //Make sure the gui matches the selected mode
            eParams.LoadBottomUpDefaults();
            UpdateScreenParams();
        }
 
        private void kryptonButtonInputDir_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            kryptonTextBoxInputDir.Text = folderBrowserDialog1.SelectedPath;
        }

        private void kryptonButtonOutputDir_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            kryptonTextBoxOutputDir.Text = folderBrowserDialog1.SelectedPath;
        }

        private void GO_Button_Click(object sender, EventArgs e)
        {
            //Do some error checking
            if (kryptonTextBoxInputDir.Text.Length == 0 || kryptonTextBoxOutputDir.Text.Length == 0)
            {
                ErrorWindow ew = new ErrorWindow("Please provide and input and output directory.");
                ew.ShowDialog();
                return;
            }

            if (kryptonTextBoxInputDir.Text.Equals(kryptonTextBoxOutputDir.Text))
            {
                ErrorWindow ew = new ErrorWindow("The input and output directory cannot be the same.");
                ew.ShowDialog();
                return;
            }

            kryptonButtonDDGo.Text = "Im busy!";
            kryptonButtonDDGo.Enabled = false;
            groupBoxWhatToDo.Enabled = false;

            //Free the GUI
            backgroundWorker1.RunWorkerAsync();
            
        }

        //--------------------------------------------------------------------------

        private ExecutionParams GetEParamsFromScreen()
        {
            
            eParams.SetMyParams(
                (double)kryptonNumericUpDownMS1MinIntensityz.Value,              //MS1 Minimum Intensity
                (double)kryptonNumericUpDownMS2MinIntensity.Value,              //MS2 Minimum Intensity
                (double)kryptonNumericUpDownPPM.Value,                          //PPM
                (int)kryptonNumericUpDownMaxCharge.Value,                       //Max Charge State
                (int)kryptonNumericUpDownMinDechargedPeaks.Value,               //Min Decharged Peaks
                kryptonCheckBoxFTMS.Checked,                                    //Print FTMS
                kryptonCheckBoxITMS.Checked,                                    //Print ITMS
                (double)kryptonNumericUpDownStringencyMS1.Value,
                (double)kryptonNumericUpDownStringencyMS2.Value,                   //Stringency
                (double)kryptonNumericUpDownConvolutedClusteringFacotr.Value,   //ConvolutedClusteringFactor
                (double)kryptonNumericUpDownDeconvolutedClusteringFactor.Value, //DeconvolutedClusteringFactor
                (double)kryptonNumericUpDownStringencyMS1Correction.Value,      //We need to be more strict wen correcting monoisotopic envelopes
                (int)kryptonNumericUpDownMaxNoPeaks.Value,                      //If we have more than the maximum number of alowed peaks, the ones of least stringency will be eliminated
                (bool)kryptonCheckBoxFTFiltering.Checked,             //The FT Filtering
                (double)kryptonNumericUpDownFilteringPPM.Value,
                (double)kryptonNumericUpDownToleranceForMultipleEnvelopes.Value,
                kryptonCheckBoxMS1CorrSkipPlusOnes.Checked,
                kryptonCheckBoxGaussianFilter.Checked,
                kryptonCheckBoxUndetectedPrecursors.Checked,
                kryptonCheckBoxSkipPlusOnesInMS2.Checked,
                kryptonCheckBoxPreserveUniqueXtractResults.Checked,
                radioButtonFormatMS1MS2.Checked,
                (int)numericUpDownMaxNoThreads.Value,
                (bool)kryptonCheckBoxSkipPlusOneInMS1.Checked,
                (bool)checkBoxDeconvoluteMS2.Checked
                );

            eParams.Decharged = checkBoxDecharge.Checked;
            return eParams;
        }

        private void UpdateScreenParams()
        {
                kryptonNumericUpDownMS1MinIntensityz.Value = (decimal)eParams.MS1MinIntensity;              //MS1 Minimum Intensity
                kryptonNumericUpDownMS2MinIntensity.Value = (decimal) eParams.MS2MinIntensity;              //MS2 Minimum Intensity
                kryptonNumericUpDownPPM.Value = (decimal) eParams.PPM;                          //PPM
                kryptonNumericUpDownMaxCharge.Value = (decimal)eParams.MaxCharge;                       //Max Charge State
                kryptonNumericUpDownMinDechargedPeaks.Value = (decimal)eParams.MinDechargedPeaks;               //Min Decharged Peaks
                kryptonCheckBoxFTMS.Checked = eParams.PrintFTMS;                                    //Print FTMS
                kryptonCheckBoxITMS.Checked = eParams.PrintITMS;                                    //Print ITMS
                kryptonNumericUpDownStringencyMS1.Value = (decimal)eParams.StringencyMS1;
                kryptonNumericUpDownStringencyMS2.Value = (decimal)eParams.StringencyMS2;                   //Stringency
                kryptonNumericUpDownConvolutedClusteringFacotr.Value = (decimal)eParams.ConvolutedClusteringFactor;   //ConvolutedClusteringFactor
                kryptonNumericUpDownDeconvolutedClusteringFactor.Value = (decimal)eParams.DeconvolutedClusteringFactor; //DeconvolutedClusteringFactor
                kryptonNumericUpDownStringencyMS1Correction.Value = (decimal)eParams.StringencyMS1Correction;      //We need to be more strict wen correcting monoisotopic envelopes
                kryptonNumericUpDownMaxNoPeaks.Value = (decimal)eParams.MaxNoPeaks;                      //If we have more than the maximum number of alowed peaks, the ones of least stringency will be eliminated
                kryptonCheckBoxFTFiltering.Checked = eParams.FTMSFiltering;             //The FT Filtering
                kryptonNumericUpDownFilteringPPM.Value = (decimal)eParams.FTMSFilteringPPM;
                kryptonNumericUpDownToleranceForMultipleEnvelopes.Value = (decimal)eParams.ToleranceMultiplexCorrection;
                kryptonCheckBoxMS1CorrSkipPlusOnes.Checked = eParams.SkipPlusOnesMS1Corr;
                kryptonCheckBoxGaussianFilter.Checked = eParams.GaussianFilter;
                kryptonCheckBoxUndetectedPrecursors.Checked = eParams.UndetectedPrecursors;
                kryptonCheckBoxSkipPlusOnesInMS2.Checked = eParams.SkipPlusOnesInMS2;
                kryptonCheckBoxSkipPlusOneInMS1.Checked = eParams.SkipPlusOneInMS1;
                kryptonCheckBoxPreserveUniqueXtractResults.Checked = eParams.PreserveUniqueXtractResults;
                this.Update();
        }

        //------------------------------------------------------------------------

        private void ExecuteSpectralProcessing(ExecutionParams eParams)
        {
            //Check if there are only raw files, if so, lets extract to the MS1 and MS2 format
            DirectoryInfo di = new DirectoryInfo(kryptonTextBoxInputDir.Text);
            List<FileInfo> ms2OrMGF = di.GetFiles("*.ms2").ToList();
            ms2OrMGF.AddRange(di.GetFiles("*.mgf"));

            if (ms2OrMGF.Count == 0 && di.GetFiles("*.raw").Count() > 0)
            {
                RawReaderParams rawReaderParams = new RawReaderParams();

                if (kryptonRadioButtonDDMS2usingMS1.Checked || kryptonRadioButtonDDMS1andMS2.Checked || kryptonRadioButtonDDMS1.Checked)
                {
                    rawReaderParams.ExtractMS1 = true;
                }
                rawReaderParams.ExtractMS2 = true;
                rawReaderParams.UseThermoMonoIsotopicPrediction = true;

                

                foreach (FileInfo fi in di.GetFiles("*.raw"))
                {

                    Console.WriteLine("Extracting spectra from file :: " + fi.Name);

                    PatternTools.RawReader.Reader reader = new PatternTools.RawReader.Reader(rawReaderParams);

                    List<MSLight> theSpectra = reader.GetSpectra(fi.FullName, new List<int>(), false);
                    //Print the MS1
                    List<MSLight> ms2PrintMS1 = theSpectra.FindAll(a => a.ZLines.Count == 0);
                    SpectraPrinter.PrintFile(fi, ms2PrintMS1, ".ms1", rawReaderParams);

                    //Print the MS2
                    List<MSLight> ms2PrintMS2 = theSpectra.FindAll(a => a.ZLines.Count > 0);
                    SpectraPrinter.PrintFile(fi, ms2PrintMS2, ".ms2", rawReaderParams);
                }
            }

            //Done extracting from .raw-------------------------------------------------


            if (kryptonRadioButtonDDMS2usingMS1.Checked)
            {
                decharger.DechargeFileExecuteOption1(eParams, DeisotopeTools.GetMSFilePairs(kryptonTextBoxInputDir.Text), kryptonTextBoxOutputDir.Text, checkBoxDeconvoluteMS2.Checked);
            }
            else
            {
                bool DDMS1 = false;
                bool DDMS2 = false;

                if (kryptonRadioButtonDDMS1andMS2.Checked)
                {
                    DDMS1 = true;
                    DDMS2 = true;
                }
                else if (kryptonRadioButtonDDMS2.Checked)
                {
                    DDMS2 = true;
                }
                else
                {
                    DDMS1 = true;
                }

                decharger.DechargeFileExecuteOption1or2(eParams,
                kryptonTextBoxInputDir.Text,
                kryptonTextBoxOutputDir.Text,
                DDMS1,
                DDMS2,
                checkBoxDecharge.Checked
                );
                
            }


        }


        

        private void MemEfficientPart1(out string tmpDir, out List<FileInfo> originalFiles)
        {
            tmpDir = kryptonTextBoxInputDir.Text + "/tmp";
            Directory.CreateDirectory(tmpDir);
            Directory.CreateDirectory(tmpDir + "/work");

            string inputDir = kryptonTextBoxInputDir.Text;
            DirectoryInfo di = new DirectoryInfo(inputDir);
            originalFiles = di.GetFiles("*.ms2").ToList();
            originalFiles.AddRange(di.GetFiles("*.ms1").ToList());


            List<DeisotopeTools.FilePairs> filePairs = DeisotopeTools.GetMSFilePairs(kryptonTextBoxInputDir.Text);

            foreach (DeisotopeTools.FilePairs couplet in filePairs)
            {
                Console.WriteLine("Chopping couplet for " + couplet.MS1.Name);
                MSChopper.ChopCouplet(couplet, 500, tmpDir);
            }
        }

        

        //--------------------------------------------------------------------

        private void kryptonButtonFeelingLucky_Click(object sender, EventArgs e)
        {
            MessageBox.Show("I bet you are!");
        }


        private void kryptonButtonSaveParameters_Click_1(object sender, EventArgs e)
        {
            ExecutionParams eParams = GetEParamsFromScreen();

            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                eParams.SaveParams(saveFileDialog1.FileName);
            }

            MessageBox.Show("File " + saveFileDialog1.FileName + " saved");
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                ExecutionParams eParams = GetEParamsFromScreen();
                long t1 = DateTime.Now.Ticks;
                ExecuteSpectralProcessing(eParams);
                long t2 = DateTime.Now.Ticks;
                Console.WriteLine("Benchmarking.  Time for DD (disconsidering parsing & filtering: " + TimeSpan.FromTicks(t2 - t1));
            }
            catch (Exception e3)
            {
                ErrorWindow ew = new ErrorWindow(e3.Message);
                ew.ShowDialog();
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            kryptonButtonDDGo.Text = "GO!";
            kryptonButtonDDGo.Enabled = true;
            groupBoxWhatToDo.Enabled = true;
            this.Update();
        }


        private void kryptonCheckButtonBottumUp_Click(object sender, EventArgs e)
        {
            eParams.LoadBottomUpDefaults();
            UpdateScreenParams();

        }

        private void kryptonCheckButtonMiddleDown_Click(object sender, EventArgs e)
        {
            eParams.LoadMiddleDownDefaults();
            UpdateScreenParams();
        }

        private void kryptonCheckButtonTopDown_Click(object sender, EventArgs e)
        {
            eParams.LoadTopDownpDefaults();
            UpdateScreenParams();
        }


        private void kryptonCheckButtonGeekMode_CheckedChanged(object sender, EventArgs e)
        {
            if (kryptonCheckButtonGeekMode.Checked)
            {
                groupBoxControlPanel.Enabled = true;
            }
            else
            {
                groupBoxControlPanel.Enabled = false;
            }
        }

        private void kryptonCheckButtonTopDonw_Click(object sender, EventArgs e)
        {
            eParams.LoadTopDownpDefaults();
            UpdateScreenParams();
        }

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            PatternTools.WebBrowser.Browser b = new PatternTools.WebBrowser.Browser();
            b.SetURL("http://bioinformatics.oxfordjournals.org/cgi/content/full/25/20/2734");
            b.ShowDialog();
            
        }

        private void kryptonRadioButtonDDMS1andMS2_CheckedChanged(object sender, EventArgs e)
        {
            if (kryptonRadioButtonDDMS1andMS2.Checked)
            {
                radioButtonFormatMGF.Enabled = false;
                radioButtonFormatMGF.Checked = false;
                radioButtonFormatMS1MS2.Checked = true;

            }
            else
            {
                radioButtonFormatMGF.Enabled = true;
            }
        }

        private void isotopicFinder2_Load(object sender, EventArgs e)
        {

        }

    }
}
