﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PatternTools.MSParser.Deisotoping;
using System.IO;

namespace PatternTools.MSChopper
{
    public partial class MSChopperControl : UserControl
    {
        public MSChopperControl()
        {
            InitializeComponent();
        }

        private void kryptonButtonGo_Click(object sender, EventArgs e)
        {

            //Some basic error checking
            if (kryptonTextBoxInput.Text.Equals("") || kryptonTextBoxOutput.Text.Equals(""))
            {
                PatternTools.ErrorWindow ew = new ErrorWindow("Please specify the input and output directories.");
                ew.ShowDialog();
                return;
            }

            if (kryptonTextBoxInput.Text.Equals(kryptonTextBoxOutput.Text))
            {
                ErrorWindow ew = new ErrorWindow("Please specify different input and output directories");
                ew.ShowDialog();
                return;
            }

            //Check if the output directory exists; if it doesnt; show a dialog box to create it
            DirectoryInfo di = new DirectoryInfo(kryptonTextBoxOutput.Text);
            if (!di.Exists)
            {
                string response = MessageBox.Show("The specified output directory does not exist.  Should I create it? (if not, this process will be terminated)", "Create Directory", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString();
                if (response.Equals("Yes"))
                {
                    di.Create();
                }
                else
                {
                    return;
                }
            }


            //Get the work done!
            try
            {
                List<DeisotopeTools.FilePairs> filePairs = DeisotopeTools.GetMSFilePairs(kryptonTextBoxInput.Text);

                kryptonButtonGo.Text = "Im busy!";
                this.Update();

                foreach (DeisotopeTools.FilePairs couplet in filePairs)
                {
                    Console.WriteLine("Chopping couplet for " + couplet.MS1.Name);
                    MSChopper.ChopCouplet(couplet, (int)kryptonNumericUpDownMS1PerCouplet.Value, kryptonTextBoxOutput.Text);
                }
            }
            catch (Exception e2)
            {
                PatternTools.ErrorWindow ew = new ErrorWindow(e2.Message);
                ew.ShowDialog();
            }
            Console.WriteLine("Done chopping couplets.");

            kryptonButtonGo.Text = "Go !";

        }

        private void kryptonButtonBrowseInput_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() != DialogResult.Cancel)
            {
                kryptonTextBoxInput.Text = folderBrowserDialog1.SelectedPath;
            }
                
        }

        private void kryptonButtonBrowseOutput_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() != DialogResult.Cancel)
            {
                kryptonTextBoxOutput.Text = folderBrowserDialog1.SelectedPath;
            }
        }
    }
}
