﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using PatternTools.MSParser.Deisotoping;

namespace PatternTools.MSChopper
{
    public static class MSChopper
    {
        public static void ChopCouplet(DeisotopeTools.FilePairs couplet, int noMS1s, string outputDirectory)
        {
            Regex startsWithS = new Regex("^S", RegexOptions.Compiled);

            StreamReader readerMS1 = new StreamReader(couplet.MS1.FullName);
            StreamReader readerMS2 = new StreamReader(couplet.MS2.FullName);

            StreamWriter writerMS1 = new StreamWriter(outputDirectory + "/" + "/00_" + couplet.MS1.Name);
            StreamWriter writerMS2 = new StreamWriter(outputDirectory + "/" + "/00_" + couplet.MS2.Name);

            int totalMS1SpectrumCounter = 0;
            int partialMS1SpectrumCounter = 0;
            int segmentCounter = 0;

            string nextMS1Line = "";
            string nextMS2Line = "";

            List<int> MS1FirstScanNumbers = new List<int>();

            //Print the MS1's
            while ( (nextMS1Line = readerMS1.ReadLine()) != null) {

                if (startsWithS.IsMatch(nextMS1Line))
                {
                    partialMS1SpectrumCounter++;
                    totalMS1SpectrumCounter++;

                    Console.WriteLine(totalMS1SpectrumCounter);

                    if (partialMS1SpectrumCounter == noMS1s)
                    {
                        string[] cols = Regex.Split(nextMS1Line, "\t");
                        MS1FirstScanNumbers.Add(int.Parse(cols[1]));

                        partialMS1SpectrumCounter = 0;
                        segmentCounter++;

                        writerMS1.Close();

                        writerMS1 = new StreamWriter(outputDirectory + "/" + segmentCounter.ToString("00") + "_" + couplet.MS1.Name);
                    }

                }

                writerMS1.WriteLine(nextMS1Line);

            }
            MS1FirstScanNumbers.Add(int.MaxValue);

            //Release some stuff
            readerMS1.Close();
            writerMS1.Flush();
            writerMS1.Close();

        //    //Print the MS2's
            int ms1Position = 0;
            segmentCounter = 0;
            while ((nextMS2Line = readerMS2.ReadLine()) != null)
            {

                if (startsWithS.IsMatch(nextMS2Line))
                {
                    //Find out the Scan number
                    string[] cols = Regex.Split(nextMS2Line, "\t");
                    int ms2ScanNumber = int.Parse(cols[1]);

                    if (ms2ScanNumber > MS1FirstScanNumbers[ms1Position])
                    {
                        ms1Position++;
                        segmentCounter++;
                        writerMS2.Close();
                        writerMS2 = new StreamWriter(outputDirectory + "/" + segmentCounter.ToString("00") + "_" + couplet.MS2.Name);
                    }
                }

                writerMS2.WriteLine(nextMS2Line);
            }

            //Release some stuff
            readerMS2.Close();
            writerMS2.Flush();
            writerMS2.Close();
            
        }
    }
}
