﻿namespace PatternTools.MSChopper
{
    partial class MSChopperControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MSChopperControl));
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonTextBoxInput = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonTextBoxOutput = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonButtonBrowseInput = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButtonBrowseOutput = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonNumericUpDownMS1PerCouplet = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonButtonGo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBoxMSChopper = new System.Windows.Forms.GroupBox();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBoxMSChopper.SuspendLayout();
            this.SuspendLayout();
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(81, 19);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(89, 20);
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Text = "InputDirectory";
            this.kryptonLabel1.Values.ExtraText = "";
            this.kryptonLabel1.Values.Image = null;
            this.kryptonLabel1.Values.Text = "InputDirectory";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(71, 50);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(99, 20);
            this.kryptonLabel2.TabIndex = 1;
            this.kryptonLabel2.Text = "OutputDirectory";
            this.kryptonLabel2.Values.ExtraText = "";
            this.kryptonLabel2.Values.Image = null;
            this.kryptonLabel2.Values.Text = "OutputDirectory";
            // 
            // kryptonTextBoxInput
            // 
            this.kryptonTextBoxInput.Location = new System.Drawing.Point(176, 19);
            this.kryptonTextBoxInput.Name = "kryptonTextBoxInput";
            this.kryptonTextBoxInput.Size = new System.Drawing.Size(194, 20);
            this.kryptonTextBoxInput.TabIndex = 2;
            // 
            // kryptonTextBoxOutput
            // 
            this.kryptonTextBoxOutput.Location = new System.Drawing.Point(176, 50);
            this.kryptonTextBoxOutput.Name = "kryptonTextBoxOutput";
            this.kryptonTextBoxOutput.Size = new System.Drawing.Size(194, 20);
            this.kryptonTextBoxOutput.TabIndex = 3;
            // 
            // kryptonButtonBrowseInput
            // 
            this.kryptonButtonBrowseInput.Location = new System.Drawing.Point(376, 14);
            this.kryptonButtonBrowseInput.Name = "kryptonButtonBrowseInput";
            this.kryptonButtonBrowseInput.Size = new System.Drawing.Size(90, 25);
            this.kryptonButtonBrowseInput.TabIndex = 4;
            this.kryptonButtonBrowseInput.Text = "Browse";
            this.kryptonButtonBrowseInput.Values.ExtraText = "";
            this.kryptonButtonBrowseInput.Values.Image = null;
            this.kryptonButtonBrowseInput.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonBrowseInput.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonBrowseInput.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonBrowseInput.Values.Text = "Browse";
            this.kryptonButtonBrowseInput.Click += new System.EventHandler(this.kryptonButtonBrowseInput_Click);
            // 
            // kryptonButtonBrowseOutput
            // 
            this.kryptonButtonBrowseOutput.Location = new System.Drawing.Point(376, 45);
            this.kryptonButtonBrowseOutput.Name = "kryptonButtonBrowseOutput";
            this.kryptonButtonBrowseOutput.Size = new System.Drawing.Size(90, 25);
            this.kryptonButtonBrowseOutput.TabIndex = 5;
            this.kryptonButtonBrowseOutput.Text = "Browse";
            this.kryptonButtonBrowseOutput.Values.ExtraText = "";
            this.kryptonButtonBrowseOutput.Values.Image = null;
            this.kryptonButtonBrowseOutput.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonBrowseOutput.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonBrowseOutput.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonBrowseOutput.Values.Text = "Browse";
            this.kryptonButtonBrowseOutput.Click += new System.EventHandler(this.kryptonButtonBrowseOutput_Click);
            // 
            // kryptonNumericUpDownMS1PerCouplet
            // 
            this.kryptonNumericUpDownMS1PerCouplet.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMS1PerCouplet.Location = new System.Drawing.Point(176, 76);
            this.kryptonNumericUpDownMS1PerCouplet.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMS1PerCouplet.Name = "kryptonNumericUpDownMS1PerCouplet";
            this.kryptonNumericUpDownMS1PerCouplet.Size = new System.Drawing.Size(194, 22);
            this.kryptonNumericUpDownMS1PerCouplet.TabIndex = 6;
            this.kryptonNumericUpDownMS1PerCouplet.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(15, 76);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(155, 20);
            this.kryptonLabel3.TabIndex = 7;
            this.kryptonLabel3.Text = "# MS1 Spectrum / Couplet";
            this.kryptonLabel3.Values.ExtraText = "";
            this.kryptonLabel3.Values.Image = null;
            this.kryptonLabel3.Values.Text = "# MS1 Spectrum / Couplet";
            // 
            // kryptonButtonGo
            // 
            this.kryptonButtonGo.Location = new System.Drawing.Point(15, 104);
            this.kryptonButtonGo.Name = "kryptonButtonGo";
            this.kryptonButtonGo.Size = new System.Drawing.Size(451, 25);
            this.kryptonButtonGo.TabIndex = 8;
            this.kryptonButtonGo.Text = "GO";
            this.kryptonButtonGo.Values.ExtraText = "";
            this.kryptonButtonGo.Values.Image = null;
            this.kryptonButtonGo.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonGo.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonGo.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonGo.Values.Text = "GO";
            this.kryptonButtonGo.Click += new System.EventHandler(this.kryptonButtonGo_Click);
            // 
            // groupBoxMSChopper
            // 
            this.groupBoxMSChopper.Controls.Add(this.kryptonLabel1);
            this.groupBoxMSChopper.Controls.Add(this.kryptonButtonGo);
            this.groupBoxMSChopper.Controls.Add(this.kryptonLabel2);
            this.groupBoxMSChopper.Controls.Add(this.kryptonLabel3);
            this.groupBoxMSChopper.Controls.Add(this.kryptonTextBoxInput);
            this.groupBoxMSChopper.Controls.Add(this.kryptonNumericUpDownMS1PerCouplet);
            this.groupBoxMSChopper.Controls.Add(this.kryptonTextBoxOutput);
            this.groupBoxMSChopper.Controls.Add(this.kryptonButtonBrowseOutput);
            this.groupBoxMSChopper.Controls.Add(this.kryptonButtonBrowseInput);
            this.groupBoxMSChopper.Location = new System.Drawing.Point(3, 48);
            this.groupBoxMSChopper.Name = "groupBoxMSChopper";
            this.groupBoxMSChopper.Size = new System.Drawing.Size(486, 146);
            this.groupBoxMSChopper.TabIndex = 9;
            this.groupBoxMSChopper.TabStop = false;
            this.groupBoxMSChopper.Text = "Parameters";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.Size = new System.Drawing.Size(511, 31);
            this.kryptonHeader1.TabIndex = 10;
            this.kryptonHeader1.Text = "MSChopper";
            this.kryptonHeader1.Values.Description = "Breaks down large MS files";
            this.kryptonHeader1.Values.Heading = "MSChopper";
            this.kryptonHeader1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonHeader1.Values.Image")));
            // 
            // MSChopperControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.kryptonHeader1);
            this.Controls.Add(this.groupBoxMSChopper);
            this.Name = "MSChopperControl";
            this.Size = new System.Drawing.Size(511, 234);
            this.groupBoxMSChopper.ResumeLayout(false);
            this.groupBoxMSChopper.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox kryptonTextBoxInput;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox kryptonTextBoxOutput;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonBrowseInput;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonBrowseOutput;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMS1PerCouplet;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonGo;
        private System.Windows.Forms.GroupBox groupBoxMSChopper;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}
