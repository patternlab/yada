﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Linq;
using PatternTools.MSParser;
using System.Threading;

namespace YADA
{
    public class Decharger
    {
        Regex RegexFTMS = new Regex(@"FTMS", RegexOptions.Compiled);
        Regex RegexITMS = new Regex(@"ITMS", RegexOptions.Compiled);
        List<PatternTools.MSParser.Deisotoping.IsotopicStep> isotopicSteps = new List<PatternTools.MSParser.Deisotoping.IsotopicStep>();

        public Decharger()
        {
            for (int i = 0; i < 25; i++)
            {
                isotopicSteps.Add(PatternTools.MSParser.Deisotoping.IsotopicSignalGenerator.GenerateIsotopicSteps(i, 9));
            }
        }

        /// <summary>
        /// Combo rendundant options as to the Decharge file method
        /// </summary>
        /// <param name="eParams"></param>
        /// <param name="filePairs"></param>
        /// <param name="outputDir"></param>
        public void DechargeFileExecuteOption1(ExecutionParams eParams, List<PatternTools.MSParser.Deisotoping.DeisotopeTools.FilePairs> filePairs, string outputDir, bool deconvoluteMS2)
        {
            foreach (PatternTools.MSParser.Deisotoping.DeisotopeTools.FilePairs f in filePairs)
            {
                Console.WriteLine("Parsing couplet for: " + f.MS1.Name);
                PatternTools.MSParser.MSMultiSpectraParser parserMS = new MSMultiSpectraParser();

                //Parse the MS1 file
                Console.WriteLine("\tParsing & Filtering " + f.MS1.Name);
                parserMS.parseFileMS1(f.MS1, 3, eParams.MS1MinIntensity, eParams.FTMSFiltering, eParams.FTMSFilteringPPM, eParams.GaussianFilter);

                //Parse the MS2 file
                Console.WriteLine("\tParsing" + f.MS2.Name);
                parserMS.parseFileMS2(f.MS2, 3, eParams.MS2MinIntensity, eParams.FTMSFiltering, eParams.FTMSFilteringPPM);

                string outputFileName = f.MS2.Name;
                if (!eParams.FormatIsMS1MS2)
                {
                    outputFileName = Regex.Replace(outputFileName, @"\.ms2", ".mgf");
                }

                StreamWriter output = new StreamWriter(outputDir + "/" + outputFileName);

                Console.WriteLine("Decharging...");

                DeisotopeDechargeMS2FixUsingMS1(parserMS.GetAllSpectraFromAllFiles(), eParams, output, parserMS.TheFiles[1].Header);

                output.Close();
            }
        }


        /// <summary>
        /// Combo rendundant options as to the Decharge file method
        /// </summary>
        /// <param name="eParams"></param>
        /// <param name="inputDirectoryName"></param>
        /// <param name="DDMS1AndMS2"></param>
        /// <param name="outputDirectoryName"></param>
        public void DechargeFileExecuteOption1or2(ExecutionParams eParams, string inputDirectoryName, string outputDirectoryName, bool DDMS1, bool DDMS2, bool decharge)
        {
            DirectoryInfo di = new DirectoryInfo(inputDirectoryName);

            ConcurrentBag<FileInfo> fiMS = new ConcurrentBag<FileInfo>();

            if (DDMS1)
            {
                FileInfo[] theFiles = di.GetFiles("*.ms1");

                foreach (FileInfo fi in theFiles)
                {
                    fiMS.Add(fi);
                }
            }

            if (DDMS2)
            {
                if (eParams.FormatIsMS1MS2)
                {
                    foreach (FileInfo fi in di.GetFiles("*.ms2"))
                    {
                        fiMS.Add(fi);
                    } 
                }
                else
                {
                    foreach (FileInfo fi in di.GetFiles("*.mgf"))
                    {
                        fiMS.Add(fi);
                    } 
                }
                
            }

            //Lets put some limitations not to bust the computers ram
            int maxNoThreads = eParams.MaxNoThreads;
            Semaphore semaphore = new Semaphore(maxNoThreads, maxNoThreads);

            Parallel.ForEach(fiMS, f =>
            //foreach (FileInfo f in fiMS)
            {
                semaphore.WaitOne();
                eParams = CenterForDecharging(eParams, outputDirectoryName, f, decharge);
                semaphore.Release();
            }
            );

        }

        private ExecutionParams CenterForDecharging(ExecutionParams eParams, string outputDirectoryName, FileInfo f, bool decharge)
        {
            Console.WriteLine("Parsing file " + f.Name);

            string outputFileName = f.Name;
            if (!eParams.FormatIsMS1MS2)
            {
                outputFileName = Regex.Replace(outputFileName, @"\.ms2", ".mgf");
            }

            MSMultiSpectraParser parserMS = new MSMultiSpectraParser();

            //Parse the input file if it is an MS1
            List<MSFull> theFullMSSpectra = new List<MSFull>();
            string header = "";
            if (f.Extension.Equals(".MS1") || f.Extension.Equals(".ms1"))
            {
                parserMS.parseFileMS1(f, 5, eParams.MS1MinIntensity, eParams.FTMSFiltering, eParams.FTMSFilteringPPM, eParams.GaussianFilter);
                header = parserMS.TheFiles[0].Header;
                theFullMSSpectra = parserMS.GetAllSpectraFromAllFiles();
            }
            else if (f.Extension.Equals(".MS2") || f.Extension.Equals(".ms2"))
            {
                //the ftfiltering is done if thenessary in the decharging step
                parserMS.parseFileMS2(f, 5, eParams.MS2MinIntensity, eParams.FTMSFiltering, eParams.FTMSFilteringPPM);
                header = parserMS.TheFiles[0].Header;
                theFullMSSpectra = parserMS.GetAllSpectraFromAllFiles();
            }
            else if (f.Extension.Equals(".mgf"))
            {
                theFullMSSpectra = PatternTools.MSParser.MGFParser.ParseMGFFile(f.FullName);
                
            }


            StreamWriter output = new StreamWriter(outputDirectoryName + "/" + outputFileName);

            ProcessFile(theFullMSSpectra, eParams, output, header, decharge);

            Console.WriteLine("Done!");
            output.Close();
            return eParams;
        }


        //----------------------------------

        public void ProcessFile(List<PatternTools.MSParser.MSFull> theMS, ExecutionParams eParams, StreamWriter outputFile, string fileHeader, bool decharge)
        {
            //Print some ions statistics
            double[] ionStatistics = new double[eParams.MaxCharge];
            
            //
            PatternTools.MSParser.MSDeisotoper deisotoper = new PatternTools.MSParser.MSDeisotoper(eParams.PPM, eParams.MaxCharge);


            Console.WriteLine("Decharging and writing output");

            //Evaluation for ms2 precursors
            int counter = 0;

            //System.Threading.Parallel.ForEach(theMS, ms =>
            foreach (PatternTools.MSParser.MSFull ms in theMS)
            {
                counter++;

                if (RegexFTMS.IsMatch(ms.InstrumentType) && !eParams.PrintFTMS)
                {
                    //do nothing
                    Console.Write(".");

                }
                else if (RegexITMS.IsMatch(ms.InstrumentType) && !eParams.PrintITMS)
                {
                    //do nothing
                    Console.Write(".");
                }
                else
                {
                    Console.WriteLine("Scan: " + counter + " / " + theMS.Count);

                    if (RegexFTMS.IsMatch(ms.InstrumentType))
                    {
                        double stringency = eParams.StringencyMS2;
                        if (!ms.isMS2)
                        {
                            stringency = eParams.StringencyMS1;
                        }

                        List<EnvelopeScore> envelopes = deisotoper.DeIsotopeMS(ms, eParams.ConvolutedClusteringFactor, stringency, false, eParams.MaxNoPeaks, eParams.UndetectedPrecursors, eParams.SkipPlusOnesInMS2);
                        PrepareDeconvolutedMS(eParams, deisotoper, ms, envelopes, decharge);
                    }
                }
            }
            //);



            //Print all spectra
            //Write File Header
            theMS.Sort((a, b) => a.ScanNumber.CompareTo(b.ScanNumber));

            if (eParams.FormatIsMS1MS2)
            {
                SpectrumPrinter.WriteFileHeader(outputFile, eParams, fileHeader);
                foreach (PatternTools.MSParser.MSFull ms in theMS)
                {
                    if (RegexFTMS.IsMatch(ms.InstrumentType) && !eParams.PrintFTMS)
                    {
                        continue;
                    }
                    if (RegexITMS.IsMatch(ms.InstrumentType) && !eParams.PrintITMS)
                    {
                        continue;
                    }

                    if (ms.MSData.Count <= eParams.MinDechargedPeaks)
                    {
                        continue;
                    }
                    ms.MSData.Sort((a, b) => a.MZ.CompareTo(b.MZ));
                    SpectrumPrinter.PrintTheSpectrum(outputFile, ms, eParams);

                }
            }
            else
            {
                //lets output in the MGF format
                foreach (PatternTools.MSParser.MSFull ms in theMS)
                {
                    if (RegexFTMS.IsMatch(ms.InstrumentType) && !eParams.PrintFTMS)
                    {
                        continue;
                    }
                    if (RegexITMS.IsMatch(ms.InstrumentType) && !eParams.PrintITMS)
                    {
                        continue;
                    }

                    if (ms.MSData.Count <= eParams.MinDechargedPeaks)
                    {
                        continue;
                    }

                    SpectrumPrinter.PrintTheSpectrum(outputFile, ms, eParams);

                }
            }

            Console.WriteLine("Done!");
        }

        private void PrepareDeconvolutedMS(ExecutionParams eParams,  PatternTools.MSParser.MSDeisotoper deisotoper, PatternTools.MSParser.MSFull ms, List<EnvelopeScore> envelopes, bool decharge)
        {
            if (RegexFTMS.IsMatch(ms.InstrumentType))
            {
                ms.MSData.Clear(); //<------------------------
                foreach (EnvelopeScore es in envelopes)
                {
                    //We will deal with decharged data
                    Ion i;
                    if (decharge)
                    {
                        double dechargedMass = PatternTools.pTools.DechargeMSPeakToPlus1(es.MZ, es.PredictedCharge);
                        i = new Ion(Math.Round(dechargedMass, 5), Math.Round(es.SummedIonIntensityForPredictedCharge, 1), 0, 0);
                    }
                    else
                    {
                        i = new Ion(Math.Round(es.MZ, 5), Math.Round(es.SummedIonIntensityForPredictedCharge, 1), 0, 0);
                    }

                    i.Charge = (int)es.PredictedCharge;
                    i.ChargeScore = es.PredictedChargeScore;

                    ms.MSData.Add(i);
                }

                //And cluster the spectrum, like a centroiding algorithm
                if (!decharge)
                {
                    deisotoper.ClusterMSPeaks(eParams.DeconvolutedClusteringFactor, ms);
                }
            }
        }



        internal void DeisotopeDechargeMS2FixUsingMS1(List<PatternTools.MSParser.MSFull> theMS, ExecutionParams eParams, StreamWriter output, string header)
        {
            MSDeisotoper deisotoper = new MSDeisotoper(eParams.PPM, eParams.MaxCharge);

            //First lets DD all MS2
            Console.WriteLine("Decharging MS2");

            ConcurrentBag<MSFull> dechargingMS = new ConcurrentBag<MSFull>(theMS);

            Parallel.ForEach(dechargingMS, ms =>
            //foreach (PatternTools.MSParser.MSFull ms in theMS)
            {

                Console.WriteLine("S: " + ms.ScanNumber);

                if (ms.isMS2)
                {

                    if (RegexITMS.IsMatch(ms.InstrumentType) && !eParams.PrintITMS)
                    {
                        //
                    }
                    else if (RegexFTMS.IsMatch(ms.InstrumentType) && !eParams.PrintFTMS)
                    {
                        //
                    }
                    else
                    {
                        //Decharge the spectrum
                        if (eParams.DeconvoluteMS2)
                        {
                            Console.WriteLine("DD scan no: " + ms.ScanNumber + " / " + theMS.Count);
                            List<EnvelopeScore> envelopes = new List<EnvelopeScore>();
                            envelopes = deisotoper.DeIsotopeMS(ms, eParams.ConvolutedClusteringFactor, eParams.StringencyMS2, false, eParams.MaxNoPeaks, eParams.UndetectedPrecursors, eParams.SkipPlusOnesInMS2);
                            PrepareDeconvolutedMS(eParams, deisotoper, ms, envelopes, false);
                        }
                    }

                }

            }
            );

            theMS = dechargingMS.ToList();
            theMS.Sort((a, b) => a.ScanNumber.CompareTo(b.ScanNumber));


            //Now lets correct the Z Lines
            List<EnvelopeScore> latestMS1Envelopes = new List<EnvelopeScore>();
            
            //We cant go parallel here!
            SpectrumPrinter.WriteFileHeader(output, eParams, header);
            foreach (PatternTools.MSParser.MSFull ms in theMS)
            {
                if (RegexITMS.IsMatch(ms.InstrumentType) && !eParams.PrintITMS)
                {
                    continue;
                }
                else if (RegexFTMS.IsMatch(ms.InstrumentType) && !eParams.PrintFTMS)
                {
                    continue;

                }


                List<EnvelopeScore> envelopes = new List<EnvelopeScore>();

                if (ms.InstrumentType.Equals("FTMS") && !ms.isMS2)
                {
                    //We will work with a very high stringency on the MS1 so we wont pick bad stuff
                    latestMS1Envelopes = deisotoper.DeIsotopeMS(ms, eParams.ConvolutedClusteringFactor, eParams.StringencyMS1Correction, eParams.SkipPlusOnesMS1Corr, eParams.MaxNoPeaks, eParams.UndetectedPrecursors, eParams.SkipPlusOnesInMS2);
                } else {

                    
                    //Correct the Z line
                    double isolationWindow = eParams.ToleranceMultiplexCorrection;

                    List<EnvelopeScore> envelopesInIsolationWindow =  latestMS1Envelopes.FindAll(a => a.MZ > ms.ChargedPrecursor - 1 - isolationWindow && a.MZ < ms.ChargedPrecursor + isolationWindow);


                    //Now that we have the closest envelope, lets see if it overlaps the m/z line

                    if (envelopesInIsolationWindow.Count > 0) {
                        
                        bool needsToClearZLines = true;

                        List<double> addedDechargedPeaks = new List<double>();  //Just a buffer so we dont add things that are too close.

                        foreach (EnvelopeScore candidate in envelopesInIsolationWindow)
                        {
                            //we need to make the candidate envelope has an isotopic peak within the bounds of the precursor
                            bool isInBounds = MonoIsotopic(candidate, ms.ChargedPrecursor, ms.ScanNumber, eParams.ToleranceMultiplexCorrection);

                            int index = addedDechargedPeaks.FindIndex((a) => Math.Abs(a - candidate.MZ) < 0.2);


                            if (index == -1 && isInBounds)
                            {
                                if (needsToClearZLines)
                                {
                                    //If the preserveuniqueextract is true, we need to verify if there is already an ion of same charge,
                                    //if there is, then we can clear

                                    if (eParams.PreserveUniqueXtractResults)
                                    {
                                        List<int> chargesFromZLines = ms.GetChargesFromZLines();
                                        if (chargesFromZLines.Contains((int)candidate.PredictedCharge))
                                        {
                                            ms.ZLines.Clear();
                                        }
                                    }
                                    else
                                    {
                                        ms.ZLines.Clear();
                                    }

                                    needsToClearZLines = false;
                                }


                                Console.WriteLine("Precursor corrected for scan " + ms.ScanNumber);
                                double decharged = PatternTools.pTools.DechargeMSPeakToPlus1(candidate.MZ, candidate.PredictedCharge);
                                ms.ZLines.Add("Z\t" + candidate.PredictedCharge + "\t" + Math.Round(decharged,4));
                                ms.Ilines.Add("I\tYADA m/z: " + candidate.MZ + "\t M+H: " + Math.Round(decharged,4));
                                addedDechargedPeaks.Add(candidate.MZ);
                            }
                         }
                    }

                    //Prints the spectrum unconvoluted
                    //If we want to go parallel, we would need to remove this from here.
                    SpectrumPrinter.PrintTheSpectrum(output, ms, eParams);
                    output.Flush();
                }

            }


        }

        //---------------------------------------------------------------------------------------------


        /// <summary>
        /// Searches for the corresponding monoistoopic envelope and returns the monoisotopic mass
        /// </summary>
        /// <param name="e"></param>
        /// <param name="precursorMass"></param>
        /// <param name="ppm"></param>
        /// <param name="scanNumber">The scan number is passed for debugging purposes only and can be disconsidered</param>
        /// <returns></returns>
        private bool MonoIsotopic(EnvelopeScore e, double precursorMass, int scanNumber, double tolerance)
        {

            //Generate Possibilities
            bool liesWithinBounds = false;
            int predictedCharge = (int)e.PredictedCharge;
            double isotopicMZ = precursorMass;
            

            List<double> isotopicStepsForCharge = isotopicSteps[predictedCharge].Steps;
            List<double> mzs = new List<double>(isotopicStepsForCharge.Count);

            double upperbound = e.ChargeScoreList[0].SequentialPeakScore;


            for (int i = 0; i < e.ChargeScoreList[0].SequentialPeakScore; i++)
            {
                if (i < isotopicStepsForCharge.Count)
                {
                    mzs.Add(e.MZ + isotopicStepsForCharge[i]);
                }
            }

            //Verify all possibilities
            for (int i = 0; i < mzs.Count; i++)
            {
                //double ppmDist = PatternTools.pTools.PPM(precursorMass, mzs[i]);
                double delta = Math.Abs(precursorMass - mzs[i]);
                if (delta < tolerance)
                {
                    //We found the match
                    isotopicMZ = e.MZ;
                    liesWithinBounds = true;
                    break;
                }
            }

            return (liesWithinBounds);
            
        }



    }
}
