﻿using PatternTools;

namespace YADA
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.kryptonTextBoxInputDir = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonButtonOutputDir = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonTextBoxOutputDir = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonButtonInputDir = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButtonDDGo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBoxWhatToDo = new System.Windows.Forms.GroupBox();
            this.numericUpDownMaxNoThreads = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxDecharge = new System.Windows.Forms.CheckBox();
            this.checkBoxDeconvoluteMS2 = new System.Windows.Forms.CheckBox();
            this.kryptonRadioButtonDDMS1 = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.kryptonRadioButtonDDMS2usingMS1 = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.kryptonRadioButtonDDMS1andMS2 = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.kryptonRadioButtonDDMS2 = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.kryptonButtonSaveParameters = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonNumericUpDownMinDechargedPeaks = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel14 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonCheckBoxPreserveUniqueXtractResults = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonButtonFeelingLucky = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.kryptonSplitContainer1 = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.kryptonButtonAbout = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonFormatMGF = new System.Windows.Forms.RadioButton();
            this.radioButtonFormatMS1MS2 = new System.Windows.Forms.RadioButton();
            this.groupBoxStep2HowToDo = new System.Windows.Forms.GroupBox();
            this.kryptonCheckButtonTopDonw = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.kryptonCheckButtonBottumUp = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.kryptonCheckButtonGeekMode = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.kryptonCheckButtonMiddleDown = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.groupBoxControlPanel = new System.Windows.Forms.GroupBox();
            this.groupBoxPeakFilteringParameters = new System.Windows.Forms.GroupBox();
            this.kryptonCheckBoxGaussianFilter = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownMS1MinIntensityz = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonNumericUpDownMS2MinIntensity = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonNumericUpDownMaxNoPeaks = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel11 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownFilteringPPM = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonCheckBoxFTFiltering = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.groupBoxMS1CorrectionParameters = new System.Windows.Forms.GroupBox();
            this.kryptonCheckBoxUndetectedPrecursors = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonCheckBoxMS1CorrSkipPlusOnes = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonLabel13 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel10 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownStringencyMS1Correction = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBoxOutputParams = new System.Windows.Forms.GroupBox();
            this.kryptonLabel12 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonCheckBoxITMS = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonCheckBoxFTMS = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonLabel8 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBoxEnvelopeDetectionParameters = new System.Windows.Forms.GroupBox();
            this.kryptonCheckBoxSkipPlusOneInMS1 = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonCheckBoxSkipPlusOnesInMS2 = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.kryptonNumericUpDownStringencyMS1 = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownStringencyMS2 = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonNumericUpDownPPM = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonNumericUpDownMaxCharge = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel7 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownConvolutedClusteringFacotr = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel9 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonNumericUpDownDeconvolutedClusteringFactor = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.kryptonCheckSetHowToDo = new ComponentFactory.Krypton.Toolkit.KryptonCheckSet(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.isotopicFinder2 = new PatternTools.MSParser.Deisotoping.IsotopicFinder();
            this.envelopeIntensityGenerator3 = new PatternTools.MSParser.Deisotoping.EnvelopeIntensityGenerator();
            this.groupBox3.SuspendLayout();
            this.groupBoxWhatToDo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxNoThreads)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonSplitContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonSplitContainer1.Panel1)).BeginInit();
            this.kryptonSplitContainer1.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonSplitContainer1.Panel2)).BeginInit();
            this.kryptonSplitContainer1.Panel2.SuspendLayout();
            this.kryptonSplitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxStep2HowToDo.SuspendLayout();
            this.groupBoxControlPanel.SuspendLayout();
            this.groupBoxPeakFilteringParameters.SuspendLayout();
            this.groupBoxMS1CorrectionParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBoxOutputParams.SuspendLayout();
            this.groupBoxEnvelopeDetectionParameters.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.kryptonTextBoxInputDir);
            this.groupBox3.Controls.Add(this.kryptonButtonOutputDir);
            this.groupBox3.Controls.Add(this.kryptonTextBoxOutputDir);
            this.groupBox3.Controls.Add(this.kryptonButtonInputDir);
            this.groupBox3.Controls.Add(this.kryptonButtonDDGo);
            this.groupBox3.Location = new System.Drawing.Point(106, 249);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(614, 123);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Step 4: Where to do";
            // 
            // kryptonTextBoxInputDir
            // 
            this.kryptonTextBoxInputDir.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonTextBoxInputDir.Location = new System.Drawing.Point(17, 29);
            this.kryptonTextBoxInputDir.Name = "kryptonTextBoxInputDir";
            this.kryptonTextBoxInputDir.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonTextBoxInputDir.Size = new System.Drawing.Size(426, 20);
            this.kryptonTextBoxInputDir.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxInputDir.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxInputDir.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxInputDir.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxInputDir.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxInputDir.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxInputDir.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxInputDir.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxInputDir.TabIndex = 1;
            // 
            // kryptonButtonOutputDir
            // 
            this.kryptonButtonOutputDir.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonButtonOutputDir.Location = new System.Drawing.Point(449, 56);
            this.kryptonButtonOutputDir.Name = "kryptonButtonOutputDir";
            this.kryptonButtonOutputDir.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonOutputDir.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonOutputDir.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonButtonOutputDir.Size = new System.Drawing.Size(121, 25);
            this.kryptonButtonOutputDir.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonOutputDir.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonOutputDir.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonOutputDir.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonOutputDir.TabIndex = 3;
            this.kryptonButtonOutputDir.Text = "Output directory";
            this.kryptonButtonOutputDir.Values.ExtraText = "";
            this.kryptonButtonOutputDir.Values.Image = null;
            this.kryptonButtonOutputDir.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonOutputDir.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonOutputDir.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonOutputDir.Values.Text = "Output directory";
            this.kryptonButtonOutputDir.Click += new System.EventHandler(this.kryptonButtonOutputDir_Click);
            // 
            // kryptonTextBoxOutputDir
            // 
            this.kryptonTextBoxOutputDir.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonTextBoxOutputDir.Location = new System.Drawing.Point(17, 61);
            this.kryptonTextBoxOutputDir.Name = "kryptonTextBoxOutputDir";
            this.kryptonTextBoxOutputDir.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonTextBoxOutputDir.Size = new System.Drawing.Size(426, 20);
            this.kryptonTextBoxOutputDir.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxOutputDir.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxOutputDir.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxOutputDir.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxOutputDir.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxOutputDir.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxOutputDir.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonTextBoxOutputDir.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonTextBoxOutputDir.TabIndex = 4;
            // 
            // kryptonButtonInputDir
            // 
            this.kryptonButtonInputDir.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonButtonInputDir.Location = new System.Drawing.Point(449, 25);
            this.kryptonButtonInputDir.Name = "kryptonButtonInputDir";
            this.kryptonButtonInputDir.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonInputDir.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonInputDir.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonButtonInputDir.Size = new System.Drawing.Size(121, 25);
            this.kryptonButtonInputDir.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonInputDir.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonInputDir.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonInputDir.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonInputDir.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonInputDir.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonInputDir.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonInputDir.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonInputDir.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonInputDir.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonInputDir.TabIndex = 2;
            this.kryptonButtonInputDir.Text = "Input directory";
            this.kryptonButtonInputDir.Values.ExtraText = "";
            this.kryptonButtonInputDir.Values.Image = null;
            this.kryptonButtonInputDir.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonInputDir.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonInputDir.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonInputDir.Values.Text = "Input directory";
            this.kryptonButtonInputDir.Click += new System.EventHandler(this.kryptonButtonInputDir_Click);
            // 
            // kryptonButtonDDGo
            // 
            this.kryptonButtonDDGo.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonButtonDDGo.Location = new System.Drawing.Point(17, 87);
            this.kryptonButtonDDGo.Name = "kryptonButtonDDGo";
            this.kryptonButtonDDGo.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonDDGo.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonDDGo.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonButtonDDGo.Size = new System.Drawing.Size(553, 25);
            this.kryptonButtonDDGo.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonDDGo.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonDDGo.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonDDGo.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonDDGo.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonDDGo.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonDDGo.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonDDGo.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonDDGo.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonDDGo.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonDDGo.TabIndex = 5;
            this.kryptonButtonDDGo.Text = "GO !";
            this.kryptonButtonDDGo.Values.ExtraText = "";
            this.kryptonButtonDDGo.Values.Image = null;
            this.kryptonButtonDDGo.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonDDGo.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonDDGo.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonDDGo.Values.Text = "GO !";
            this.kryptonButtonDDGo.Click += new System.EventHandler(this.GO_Button_Click);
            // 
            // groupBoxWhatToDo
            // 
            this.groupBoxWhatToDo.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxWhatToDo.Controls.Add(this.numericUpDownMaxNoThreads);
            this.groupBoxWhatToDo.Controls.Add(this.label1);
            this.groupBoxWhatToDo.Controls.Add(this.checkBoxDecharge);
            this.groupBoxWhatToDo.Controls.Add(this.checkBoxDeconvoluteMS2);
            this.groupBoxWhatToDo.Controls.Add(this.kryptonRadioButtonDDMS1);
            this.groupBoxWhatToDo.Controls.Add(this.kryptonRadioButtonDDMS2usingMS1);
            this.groupBoxWhatToDo.Controls.Add(this.kryptonRadioButtonDDMS1andMS2);
            this.groupBoxWhatToDo.Controls.Add(this.kryptonRadioButtonDDMS2);
            this.groupBoxWhatToDo.Location = new System.Drawing.Point(106, 20);
            this.groupBoxWhatToDo.Name = "groupBoxWhatToDo";
            this.groupBoxWhatToDo.Size = new System.Drawing.Size(614, 112);
            this.groupBoxWhatToDo.TabIndex = 10;
            this.groupBoxWhatToDo.TabStop = false;
            this.groupBoxWhatToDo.Text = "Step 1: What to do";
            // 
            // numericUpDownMaxNoThreads
            // 
            this.numericUpDownMaxNoThreads.Location = new System.Drawing.Point(440, 81);
            this.numericUpDownMaxNoThreads.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownMaxNoThreads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaxNoThreads.Name = "numericUpDownMaxNoThreads";
            this.numericUpDownMaxNoThreads.Size = new System.Drawing.Size(73, 20);
            this.numericUpDownMaxNoThreads.TabIndex = 14;
            this.numericUpDownMaxNoThreads.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "MaxNoThreads";
            // 
            // checkBoxDecharge
            // 
            this.checkBoxDecharge.AutoSize = true;
            this.checkBoxDecharge.Checked = true;
            this.checkBoxDecharge.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDecharge.Location = new System.Drawing.Point(440, 53);
            this.checkBoxDecharge.Name = "checkBoxDecharge";
            this.checkBoxDecharge.Size = new System.Drawing.Size(73, 17);
            this.checkBoxDecharge.TabIndex = 12;
            this.checkBoxDecharge.Text = "Decharge";
            this.checkBoxDecharge.UseVisualStyleBackColor = true;
            // 
            // checkBoxDeconvoluteMS2
            // 
            this.checkBoxDeconvoluteMS2.AutoSize = true;
            this.checkBoxDeconvoluteMS2.Location = new System.Drawing.Point(440, 27);
            this.checkBoxDeconvoluteMS2.Name = "checkBoxDeconvoluteMS2";
            this.checkBoxDeconvoluteMS2.Size = new System.Drawing.Size(112, 17);
            this.checkBoxDeconvoluteMS2.TabIndex = 11;
            this.checkBoxDeconvoluteMS2.Text = "Deconvolute MS2";
            this.checkBoxDeconvoluteMS2.UseVisualStyleBackColor = true;
            // 
            // kryptonRadioButtonDDMS1
            // 
            this.kryptonRadioButtonDDMS1.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonRadioButtonDDMS1.Location = new System.Drawing.Point(145, 76);
            this.kryptonRadioButtonDDMS1.Name = "kryptonRadioButtonDDMS1";
            this.kryptonRadioButtonDDMS1.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonRadioButtonDDMS1.Size = new System.Drawing.Size(133, 20);
            this.kryptonRadioButtonDDMS1.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1.TabIndex = 10;
            this.kryptonRadioButtonDDMS1.Text = "Deisotope only MS1";
            this.kryptonRadioButtonDDMS1.Values.ExtraText = "";
            this.kryptonRadioButtonDDMS1.Values.Image = null;
            this.kryptonRadioButtonDDMS1.Values.Text = "Deisotope only MS1";
            // 
            // kryptonRadioButtonDDMS2usingMS1
            // 
            this.kryptonRadioButtonDDMS2usingMS1.Checked = true;
            this.kryptonRadioButtonDDMS2usingMS1.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonRadioButtonDDMS2usingMS1.Location = new System.Drawing.Point(6, 24);
            this.kryptonRadioButtonDDMS2usingMS1.Name = "kryptonRadioButtonDDMS2usingMS1";
            this.kryptonRadioButtonDDMS2usingMS1.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonRadioButtonDDMS2usingMS1.Size = new System.Drawing.Size(384, 20);
            this.kryptonRadioButtonDDMS2usingMS1.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2usingMS1.TabIndex = 9;
            this.kryptonRadioButtonDDMS2usingMS1.Text = "Use MS1 to assign monoisotopic envelope peaks and multiplexing";
            this.kryptonRadioButtonDDMS2usingMS1.Values.ExtraText = "";
            this.kryptonRadioButtonDDMS2usingMS1.Values.Image = null;
            this.kryptonRadioButtonDDMS2usingMS1.Values.Text = "Use MS1 to assign monoisotopic envelope peaks and multiplexing";
            // 
            // kryptonRadioButtonDDMS1andMS2
            // 
            this.kryptonRadioButtonDDMS1andMS2.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonRadioButtonDDMS1andMS2.Location = new System.Drawing.Point(6, 50);
            this.kryptonRadioButtonDDMS1andMS2.Name = "kryptonRadioButtonDDMS1andMS2";
            this.kryptonRadioButtonDDMS1andMS2.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonRadioButtonDDMS1andMS2.Size = new System.Drawing.Size(158, 20);
            this.kryptonRadioButtonDDMS1andMS2.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS1andMS2.TabIndex = 7;
            this.kryptonRadioButtonDDMS1andMS2.Text = "Deisotope MS1 and MS2";
            this.kryptonRadioButtonDDMS1andMS2.Values.ExtraText = "";
            this.kryptonRadioButtonDDMS1andMS2.Values.Image = null;
            this.kryptonRadioButtonDDMS1andMS2.Values.Text = "Deisotope MS1 and MS2";
            this.kryptonRadioButtonDDMS1andMS2.CheckedChanged += new System.EventHandler(this.kryptonRadioButtonDDMS1andMS2_CheckedChanged);
            // 
            // kryptonRadioButtonDDMS2
            // 
            this.kryptonRadioButtonDDMS2.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonRadioButtonDDMS2.Location = new System.Drawing.Point(6, 76);
            this.kryptonRadioButtonDDMS2.Name = "kryptonRadioButtonDDMS2";
            this.kryptonRadioButtonDDMS2.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonRadioButtonDDMS2.Size = new System.Drawing.Size(133, 20);
            this.kryptonRadioButtonDDMS2.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonRadioButtonDDMS2.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonRadioButtonDDMS2.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonRadioButtonDDMS2.TabIndex = 8;
            this.kryptonRadioButtonDDMS2.Text = "Deisotope only MS2";
            this.kryptonRadioButtonDDMS2.Values.ExtraText = "";
            this.kryptonRadioButtonDDMS2.Values.Image = null;
            this.kryptonRadioButtonDDMS2.Values.Text = "Deisotope only MS2";
            // 
            // kryptonButtonSaveParameters
            // 
            this.kryptonButtonSaveParameters.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonButtonSaveParameters.Location = new System.Drawing.Point(292, 20);
            this.kryptonButtonSaveParameters.Name = "kryptonButtonSaveParameters";
            this.kryptonButtonSaveParameters.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonSaveParameters.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonSaveParameters.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonButtonSaveParameters.Size = new System.Drawing.Size(149, 56);
            this.kryptonButtonSaveParameters.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonSaveParameters.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonSaveParameters.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonSaveParameters.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonSaveParameters.TabIndex = 25;
            this.kryptonButtonSaveParameters.Text = "Generate parameter file";
            this.toolTip1.SetToolTip(this.kryptonButtonSaveParameters, "This is used to generate a parameter file for the command line version");
            this.kryptonButtonSaveParameters.Values.ExtraText = "";
            this.kryptonButtonSaveParameters.Values.Image = null;
            this.kryptonButtonSaveParameters.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonSaveParameters.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonSaveParameters.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonSaveParameters.Values.Text = "Generate parameter file";
            this.kryptonButtonSaveParameters.Click += new System.EventHandler(this.kryptonButtonSaveParameters_Click_1);
            // 
            // kryptonNumericUpDownMinDechargedPeaks
            // 
            this.kryptonNumericUpDownMinDechargedPeaks.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownMinDechargedPeaks.Location = new System.Drawing.Point(225, 54);
            this.kryptonNumericUpDownMinDechargedPeaks.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.kryptonNumericUpDownMinDechargedPeaks.Name = "kryptonNumericUpDownMinDechargedPeaks";
            this.kryptonNumericUpDownMinDechargedPeaks.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownMinDechargedPeaks.Size = new System.Drawing.Size(44, 22);
            this.kryptonNumericUpDownMinDechargedPeaks.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMinDechargedPeaks.TabIndex = 11;
            this.toolTip1.SetToolTip(this.kryptonNumericUpDownMinDechargedPeaks, "This requires the decharged spectrum to have at least this amount of envelopes");
            this.kryptonNumericUpDownMinDechargedPeaks.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownMinDechargedPeaks.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // kryptonLabel14
            // 
            this.kryptonLabel14.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel14.Location = new System.Drawing.Point(267, 19);
            this.kryptonLabel14.Name = "kryptonLabel14";
            this.kryptonLabel14.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel14.Size = new System.Drawing.Size(53, 20);
            this.kryptonLabel14.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel14.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel14.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel14.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel14.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel14.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel14.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel14.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel14.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel14.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel14.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel14.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel14.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel14.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel14.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel14.TabIndex = 19;
            this.kryptonLabel14.Text = "Str MS1";
            this.toolTip1.SetToolTip(this.kryptonLabel14, "Stringency for MS1");
            this.kryptonLabel14.Values.ExtraText = "";
            this.kryptonLabel14.Values.Image = null;
            this.kryptonLabel14.Values.Text = "Str MS1";
            // 
            // kryptonCheckBoxPreserveUniqueXtractResults
            // 
            this.kryptonCheckBoxPreserveUniqueXtractResults.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxPreserveUniqueXtractResults.Location = new System.Drawing.Point(12, 82);
            this.kryptonCheckBoxPreserveUniqueXtractResults.Name = "kryptonCheckBoxPreserveUniqueXtractResults";
            this.kryptonCheckBoxPreserveUniqueXtractResults.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxPreserveUniqueXtractResults.Size = new System.Drawing.Size(207, 20);
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxPreserveUniqueXtractResults.TabIndex = 32;
            this.kryptonCheckBoxPreserveUniqueXtractResults.Text = "Preserve unique RawXtract results";
            this.toolTip1.SetToolTip(this.kryptonCheckBoxPreserveUniqueXtractResults, "If there is an ion with a different charge state than the one predicted by YADA i" +
        "ts information will be preserved");
            this.kryptonCheckBoxPreserveUniqueXtractResults.Values.ExtraText = "";
            this.kryptonCheckBoxPreserveUniqueXtractResults.Values.Image = null;
            this.kryptonCheckBoxPreserveUniqueXtractResults.Values.Text = "Preserve unique RawXtract results";
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel5.Location = new System.Drawing.Point(52, 112);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel5.Size = new System.Drawing.Size(53, 20);
            this.kryptonLabel5.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel5.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel5.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel5.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel5.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel5.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel5.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel5.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel5.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel5.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel5.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel5.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel5.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel5.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel5.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel5.TabIndex = 2;
            this.kryptonLabel5.Text = "Y.A.D.A.";
            this.kryptonLabel5.Values.ExtraText = "";
            this.kryptonLabel5.Values.Image = null;
            this.kryptonLabel5.Values.Text = "Y.A.D.A.";
            // 
            // kryptonButtonFeelingLucky
            // 
            this.kryptonButtonFeelingLucky.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonButtonFeelingLucky.Location = new System.Drawing.Point(11, 138);
            this.kryptonButtonFeelingLucky.Name = "kryptonButtonFeelingLucky";
            this.kryptonButtonFeelingLucky.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonFeelingLucky.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonFeelingLucky.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonButtonFeelingLucky.Size = new System.Drawing.Size(135, 25);
            this.kryptonButtonFeelingLucky.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonFeelingLucky.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonFeelingLucky.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonFeelingLucky.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonFeelingLucky.TabIndex = 1;
            this.kryptonButtonFeelingLucky.Text = "    I\'m feeling lucky!";
            this.kryptonButtonFeelingLucky.Values.ExtraText = "";
            this.kryptonButtonFeelingLucky.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButtonFeelingLucky.Values.Image")));
            this.kryptonButtonFeelingLucky.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonFeelingLucky.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonFeelingLucky.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonFeelingLucky.Values.Text = "    I\'m feeling lucky!";
            this.kryptonButtonFeelingLucky.Click += new System.EventHandler(this.kryptonButtonFeelingLucky_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(135, 99);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // kryptonSplitContainer1
            // 
            this.kryptonSplitContainer1.ContainerBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelClient;
            this.kryptonSplitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.kryptonSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonSplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.kryptonSplitContainer1.IsSplitterFixed = true;
            this.kryptonSplitContainer1.Location = new System.Drawing.Point(3, 3);
            this.kryptonSplitContainer1.Name = "kryptonSplitContainer1";
            this.kryptonSplitContainer1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            // 
            // kryptonSplitContainer1.Panel1
            // 
            this.kryptonSplitContainer1.Panel1.Controls.Add(this.kryptonButtonAbout);
            this.kryptonSplitContainer1.Panel1.Controls.Add(this.kryptonLabel5);
            this.kryptonSplitContainer1.Panel1.Controls.Add(this.pictureBox1);
            this.kryptonSplitContainer1.Panel1.Controls.Add(this.kryptonButtonFeelingLucky);
            this.kryptonSplitContainer1.Panel1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonSplitContainer1.Panel1.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelClient;
            this.kryptonSplitContainer1.Panel1.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.Panel1.StateDisabled.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.Panel1.StateNormal.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            // 
            // kryptonSplitContainer1.Panel2
            // 
            this.kryptonSplitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.kryptonSplitContainer1.Panel2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonSplitContainer1.Panel2.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelClient;
            this.kryptonSplitContainer1.Panel2.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.Panel2.StateDisabled.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.Panel2.StateNormal.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.SeparatorStyle = ComponentFactory.Krypton.Toolkit.SeparatorStyle.LowProfile;
            this.kryptonSplitContainer1.Size = new System.Drawing.Size(895, 422);
            this.kryptonSplitContainer1.SplitterDistance = 149;
            this.kryptonSplitContainer1.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateCommon.Separator.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateCommon.Separator.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonSplitContainer1.StateCommon.Separator.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateDisabled.Separator.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateDisabled.Separator.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonSplitContainer1.StateDisabled.Separator.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateNormal.Separator.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateNormal.Separator.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonSplitContainer1.StateNormal.Separator.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonSplitContainer1.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonSplitContainer1.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonSplitContainer1.TabIndex = 12;
            // 
            // kryptonButtonAbout
            // 
            this.kryptonButtonAbout.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonButtonAbout.Location = new System.Drawing.Point(11, 169);
            this.kryptonButtonAbout.Name = "kryptonButtonAbout";
            this.kryptonButtonAbout.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonAbout.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonAbout.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonButtonAbout.Size = new System.Drawing.Size(135, 25);
            this.kryptonButtonAbout.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonAbout.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonAbout.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonAbout.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonAbout.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonAbout.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonAbout.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonAbout.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonAbout.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonButtonAbout.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonButtonAbout.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonButtonAbout.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonButtonAbout.TabIndex = 3;
            this.kryptonButtonAbout.Text = "About / Manuscript";
            this.kryptonButtonAbout.Values.ExtraText = "";
            this.kryptonButtonAbout.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButtonAbout.Values.Image")));
            this.kryptonButtonAbout.Values.ImageStates.ImageCheckedNormal = null;
            this.kryptonButtonAbout.Values.ImageStates.ImageCheckedPressed = null;
            this.kryptonButtonAbout.Values.ImageStates.ImageCheckedTracking = null;
            this.kryptonButtonAbout.Values.Text = "About / Manuscript";
            this.kryptonButtonAbout.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBoxStep2HowToDo);
            this.groupBox1.Controls.Add(this.groupBoxWhatToDo);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(741, 422);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "All YADA\'s functionality is here.  Spectra must be in MS1 or MS2 format.";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonFormatMGF);
            this.groupBox2.Controls.Add(this.radioButtonFormatMS1MS2);
            this.groupBox2.Location = new System.Drawing.Point(106, 204);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(614, 39);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Choose the output format";
            // 
            // radioButtonFormatMGF
            // 
            this.radioButtonFormatMGF.AutoSize = true;
            this.radioButtonFormatMGF.Location = new System.Drawing.Point(211, 16);
            this.radioButtonFormatMGF.Name = "radioButtonFormatMGF";
            this.radioButtonFormatMGF.Size = new System.Drawing.Size(198, 17);
            this.radioButtonFormatMGF.TabIndex = 1;
            this.radioButtonFormatMGF.TabStop = true;
            this.radioButtonFormatMGF.Text = "Mascot Generic Format (MGF) (Beta)";
            this.radioButtonFormatMGF.UseVisualStyleBackColor = true;
            // 
            // radioButtonFormatMS1MS2
            // 
            this.radioButtonFormatMS1MS2.AutoSize = true;
            this.radioButtonFormatMS1MS2.Checked = true;
            this.radioButtonFormatMS1MS2.Location = new System.Drawing.Point(17, 16);
            this.radioButtonFormatMS1MS2.Name = "radioButtonFormatMS1MS2";
            this.radioButtonFormatMS1MS2.Size = new System.Drawing.Size(188, 17);
            this.radioButtonFormatMS1MS2.TabIndex = 0;
            this.radioButtonFormatMS1MS2.TabStop = true;
            this.radioButtonFormatMS1MS2.Text = "MS1 / MS2 (i.e., Yates lab default)";
            this.radioButtonFormatMS1MS2.UseVisualStyleBackColor = true;
            // 
            // groupBoxStep2HowToDo
            // 
            this.groupBoxStep2HowToDo.Controls.Add(this.kryptonCheckButtonTopDonw);
            this.groupBoxStep2HowToDo.Controls.Add(this.kryptonCheckButtonBottumUp);
            this.groupBoxStep2HowToDo.Controls.Add(this.kryptonCheckButtonGeekMode);
            this.groupBoxStep2HowToDo.Controls.Add(this.kryptonCheckButtonMiddleDown);
            this.groupBoxStep2HowToDo.Location = new System.Drawing.Point(106, 138);
            this.groupBoxStep2HowToDo.Name = "groupBoxStep2HowToDo";
            this.groupBoxStep2HowToDo.Size = new System.Drawing.Size(614, 59);
            this.groupBoxStep2HowToDo.TabIndex = 16;
            this.groupBoxStep2HowToDo.TabStop = false;
            this.groupBoxStep2HowToDo.Text = "Step 2: How to do (Loads the control panel\'s  default settings for a task)";
            // 
            // kryptonCheckButtonTopDonw
            // 
            this.kryptonCheckButtonTopDonw.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonCheckButtonTopDonw.Location = new System.Drawing.Point(269, 19);
            this.kryptonCheckButtonTopDonw.Name = "kryptonCheckButtonTopDonw";
            this.kryptonCheckButtonTopDonw.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckButtonTopDonw.Size = new System.Drawing.Size(120, 25);
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedPressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCheckedTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonTopDonw.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonTopDonw.TabIndex = 16;
            this.kryptonCheckButtonTopDonw.Text = "Top Down";
            this.kryptonCheckButtonTopDonw.Values.ExtraText = "";
            this.kryptonCheckButtonTopDonw.Values.Image = null;
            this.kryptonCheckButtonTopDonw.Values.Text = "Top Down";
            this.kryptonCheckButtonTopDonw.Click += new System.EventHandler(this.kryptonCheckButtonTopDonw_Click);
            // 
            // kryptonCheckButtonBottumUp
            // 
            this.kryptonCheckButtonBottumUp.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonCheckButtonBottumUp.Checked = true;
            this.kryptonCheckButtonBottumUp.Location = new System.Drawing.Point(17, 19);
            this.kryptonCheckButtonBottumUp.Name = "kryptonCheckButtonBottumUp";
            this.kryptonCheckButtonBottumUp.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckButtonBottumUp.Size = new System.Drawing.Size(120, 25);
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedPressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCheckedTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonBottumUp.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonBottumUp.TabIndex = 12;
            this.kryptonCheckButtonBottumUp.Text = "Bottum up";
            this.kryptonCheckButtonBottumUp.Values.ExtraText = "";
            this.kryptonCheckButtonBottumUp.Values.Image = null;
            this.kryptonCheckButtonBottumUp.Values.Text = "Bottum up";
            this.kryptonCheckButtonBottumUp.Click += new System.EventHandler(this.kryptonCheckButtonBottumUp_Click);
            // 
            // kryptonCheckButtonGeekMode
            // 
            this.kryptonCheckButtonGeekMode.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonCheckButtonGeekMode.Location = new System.Drawing.Point(395, 19);
            this.kryptonCheckButtonGeekMode.Name = "kryptonCheckButtonGeekMode";
            this.kryptonCheckButtonGeekMode.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckButtonGeekMode.Size = new System.Drawing.Size(120, 25);
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedPressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCheckedTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonGeekMode.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonGeekMode.TabIndex = 15;
            this.kryptonCheckButtonGeekMode.Text = "Geek Mode";
            this.kryptonCheckButtonGeekMode.Values.ExtraText = "";
            this.kryptonCheckButtonGeekMode.Values.Image = null;
            this.kryptonCheckButtonGeekMode.Values.Text = "Geek Mode";
            this.kryptonCheckButtonGeekMode.CheckedChanged += new System.EventHandler(this.kryptonCheckButtonGeekMode_CheckedChanged);
            // 
            // kryptonCheckButtonMiddleDown
            // 
            this.kryptonCheckButtonMiddleDown.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Standalone;
            this.kryptonCheckButtonMiddleDown.Location = new System.Drawing.Point(143, 19);
            this.kryptonCheckButtonMiddleDown.Name = "kryptonCheckButtonMiddleDown";
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideDefault.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.OverrideFocus.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckButtonMiddleDown.Size = new System.Drawing.Size(120, 25);
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedPressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCheckedTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateCommon.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateDisabled.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateNormal.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StatePressed.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Content.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Content.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Content.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckButtonMiddleDown.StateTracking.Content.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckButtonMiddleDown.TabIndex = 13;
            this.kryptonCheckButtonMiddleDown.Text = "Middle Down";
            this.kryptonCheckButtonMiddleDown.Values.ExtraText = "";
            this.kryptonCheckButtonMiddleDown.Values.Image = null;
            this.kryptonCheckButtonMiddleDown.Values.Text = "Middle Down";
            this.kryptonCheckButtonMiddleDown.Click += new System.EventHandler(this.kryptonCheckButtonMiddleDown_Click);
            // 
            // groupBoxControlPanel
            // 
            this.groupBoxControlPanel.Controls.Add(this.groupBoxPeakFilteringParameters);
            this.groupBoxControlPanel.Controls.Add(this.groupBoxMS1CorrectionParameters);
            this.groupBoxControlPanel.Controls.Add(this.pictureBox2);
            this.groupBoxControlPanel.Controls.Add(this.groupBoxOutputParams);
            this.groupBoxControlPanel.Controls.Add(this.groupBoxEnvelopeDetectionParameters);
            this.groupBoxControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxControlPanel.Enabled = false;
            this.groupBoxControlPanel.Location = new System.Drawing.Point(3, 3);
            this.groupBoxControlPanel.Name = "groupBoxControlPanel";
            this.groupBoxControlPanel.Size = new System.Drawing.Size(895, 422);
            this.groupBoxControlPanel.TabIndex = 39;
            this.groupBoxControlPanel.TabStop = false;
            this.groupBoxControlPanel.Text = "Configure the operating mode";
            // 
            // groupBoxPeakFilteringParameters
            // 
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonCheckBoxGaussianFilter);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonLabel1);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonLabel2);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonNumericUpDownMS1MinIntensityz);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonNumericUpDownMS2MinIntensity);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonNumericUpDownMaxNoPeaks);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonLabel11);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonNumericUpDownFilteringPPM);
            this.groupBoxPeakFilteringParameters.Controls.Add(this.kryptonCheckBoxFTFiltering);
            this.groupBoxPeakFilteringParameters.Location = new System.Drawing.Point(20, 19);
            this.groupBoxPeakFilteringParameters.Name = "groupBoxPeakFilteringParameters";
            this.groupBoxPeakFilteringParameters.Size = new System.Drawing.Size(414, 101);
            this.groupBoxPeakFilteringParameters.TabIndex = 35;
            this.groupBoxPeakFilteringParameters.TabStop = false;
            this.groupBoxPeakFilteringParameters.Text = "Peak Filtering Parameters";
            // 
            // kryptonCheckBoxGaussianFilter
            // 
            this.kryptonCheckBoxGaussianFilter.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxGaussianFilter.Location = new System.Drawing.Point(231, 19);
            this.kryptonCheckBoxGaussianFilter.Name = "kryptonCheckBoxGaussianFilter";
            this.kryptonCheckBoxGaussianFilter.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxGaussianFilter.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxGaussianFilter.Size = new System.Drawing.Size(102, 20);
            this.kryptonCheckBoxGaussianFilter.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxGaussianFilter.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxGaussianFilter.TabIndex = 27;
            this.kryptonCheckBoxGaussianFilter.Text = "Gaussian Filter";
            this.kryptonCheckBoxGaussianFilter.Values.ExtraText = "";
            this.kryptonCheckBoxGaussianFilter.Values.Image = null;
            this.kryptonCheckBoxGaussianFilter.Values.Text = "Gaussian Filter";
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel1.Location = new System.Drawing.Point(7, 19);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel1.Size = new System.Drawing.Size(109, 20);
            this.kryptonLabel1.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel1.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel1.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel1.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel1.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel1.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel1.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel1.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel1.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel1.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel1.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel1.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel1.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel1.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel1.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel1.TabIndex = 0;
            this.kryptonLabel1.Text = "MS1 Min Intensity";
            this.kryptonLabel1.Values.ExtraText = "";
            this.kryptonLabel1.Values.Image = null;
            this.kryptonLabel1.Values.Text = "MS1 Min Intensity";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel2.Location = new System.Drawing.Point(7, 45);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel2.Size = new System.Drawing.Size(109, 20);
            this.kryptonLabel2.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel2.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel2.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel2.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel2.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel2.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel2.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel2.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel2.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel2.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel2.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel2.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel2.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel2.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel2.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel2.TabIndex = 1;
            this.kryptonLabel2.Text = "MS2 Min Intensity";
            this.kryptonLabel2.Values.ExtraText = "";
            this.kryptonLabel2.Values.Image = null;
            this.kryptonLabel2.Values.Text = "MS2 Min Intensity";
            // 
            // kryptonNumericUpDownMS1MinIntensityz
            // 
            this.kryptonNumericUpDownMS1MinIntensityz.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMS1MinIntensityz.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownMS1MinIntensityz.Location = new System.Drawing.Point(153, 19);
            this.kryptonNumericUpDownMS1MinIntensityz.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMS1MinIntensityz.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kryptonNumericUpDownMS1MinIntensityz.Name = "kryptonNumericUpDownMS1MinIntensityz";
            this.kryptonNumericUpDownMS1MinIntensityz.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownMS1MinIntensityz.Size = new System.Drawing.Size(72, 22);
            this.kryptonNumericUpDownMS1MinIntensityz.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS1MinIntensityz.TabIndex = 6;
            this.kryptonNumericUpDownMS1MinIntensityz.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownMS1MinIntensityz.Value = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            // 
            // kryptonNumericUpDownMS2MinIntensity
            // 
            this.kryptonNumericUpDownMS2MinIntensity.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownMS2MinIntensity.Location = new System.Drawing.Point(153, 45);
            this.kryptonNumericUpDownMS2MinIntensity.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMS2MinIntensity.Name = "kryptonNumericUpDownMS2MinIntensity";
            this.kryptonNumericUpDownMS2MinIntensity.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownMS2MinIntensity.Size = new System.Drawing.Size(72, 22);
            this.kryptonNumericUpDownMS2MinIntensity.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMS2MinIntensity.TabIndex = 7;
            this.kryptonNumericUpDownMS2MinIntensity.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownMS2MinIntensity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // kryptonNumericUpDownMaxNoPeaks
            // 
            this.kryptonNumericUpDownMaxNoPeaks.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownMaxNoPeaks.Location = new System.Drawing.Point(337, 47);
            this.kryptonNumericUpDownMaxNoPeaks.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.kryptonNumericUpDownMaxNoPeaks.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kryptonNumericUpDownMaxNoPeaks.Name = "kryptonNumericUpDownMaxNoPeaks";
            this.kryptonNumericUpDownMaxNoPeaks.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownMaxNoPeaks.Size = new System.Drawing.Size(56, 22);
            this.kryptonNumericUpDownMaxNoPeaks.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxNoPeaks.TabIndex = 22;
            this.kryptonNumericUpDownMaxNoPeaks.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownMaxNoPeaks.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // kryptonLabel11
            // 
            this.kryptonLabel11.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel11.Location = new System.Drawing.Point(231, 47);
            this.kryptonLabel11.Name = "kryptonLabel11";
            this.kryptonLabel11.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel11.Size = new System.Drawing.Size(92, 20);
            this.kryptonLabel11.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel11.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel11.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel11.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel11.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel11.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel11.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel11.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel11.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel11.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel11.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel11.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel11.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel11.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel11.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel11.TabIndex = 21;
            this.kryptonLabel11.Text = "Max pks / spec";
            this.kryptonLabel11.Values.ExtraText = "";
            this.kryptonLabel11.Values.Image = null;
            this.kryptonLabel11.Values.Text = "Max pks / spec";
            // 
            // kryptonNumericUpDownFilteringPPM
            // 
            this.kryptonNumericUpDownFilteringPPM.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownFilteringPPM.Location = new System.Drawing.Point(153, 71);
            this.kryptonNumericUpDownFilteringPPM.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.kryptonNumericUpDownFilteringPPM.Name = "kryptonNumericUpDownFilteringPPM";
            this.kryptonNumericUpDownFilteringPPM.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownFilteringPPM.Size = new System.Drawing.Size(72, 22);
            this.kryptonNumericUpDownFilteringPPM.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownFilteringPPM.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownFilteringPPM.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownFilteringPPM.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownFilteringPPM.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownFilteringPPM.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownFilteringPPM.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownFilteringPPM.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownFilteringPPM.TabIndex = 26;
            this.kryptonNumericUpDownFilteringPPM.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownFilteringPPM.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // kryptonCheckBoxFTFiltering
            // 
            this.kryptonCheckBoxFTFiltering.Checked = true;
            this.kryptonCheckBoxFTFiltering.CheckState = System.Windows.Forms.CheckState.Checked;
            this.kryptonCheckBoxFTFiltering.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxFTFiltering.Location = new System.Drawing.Point(12, 71);
            this.kryptonCheckBoxFTFiltering.Name = "kryptonCheckBoxFTFiltering";
            this.kryptonCheckBoxFTFiltering.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTFiltering.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxFTFiltering.Size = new System.Drawing.Size(135, 20);
            this.kryptonCheckBoxFTFiltering.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTFiltering.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTFiltering.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTFiltering.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTFiltering.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTFiltering.TabIndex = 23;
            this.kryptonCheckBoxFTFiltering.Text = "FT-MS filtering PPM:";
            this.kryptonCheckBoxFTFiltering.Values.ExtraText = "";
            this.kryptonCheckBoxFTFiltering.Values.Image = null;
            this.kryptonCheckBoxFTFiltering.Values.Text = "FT-MS filtering PPM:";
            // 
            // groupBoxMS1CorrectionParameters
            // 
            this.groupBoxMS1CorrectionParameters.Controls.Add(this.kryptonCheckBoxPreserveUniqueXtractResults);
            this.groupBoxMS1CorrectionParameters.Controls.Add(this.kryptonCheckBoxUndetectedPrecursors);
            this.groupBoxMS1CorrectionParameters.Controls.Add(this.kryptonCheckBoxMS1CorrSkipPlusOnes);
            this.groupBoxMS1CorrectionParameters.Controls.Add(this.kryptonLabel13);
            this.groupBoxMS1CorrectionParameters.Controls.Add(this.kryptonNumericUpDownToleranceForMultipleEnvelopes);
            this.groupBoxMS1CorrectionParameters.Controls.Add(this.kryptonLabel10);
            this.groupBoxMS1CorrectionParameters.Controls.Add(this.kryptonNumericUpDownStringencyMS1Correction);
            this.groupBoxMS1CorrectionParameters.Location = new System.Drawing.Point(20, 234);
            this.groupBoxMS1CorrectionParameters.Name = "groupBoxMS1CorrectionParameters";
            this.groupBoxMS1CorrectionParameters.Size = new System.Drawing.Size(414, 108);
            this.groupBoxMS1CorrectionParameters.TabIndex = 38;
            this.groupBoxMS1CorrectionParameters.TabStop = false;
            this.groupBoxMS1CorrectionParameters.Text = "MS1 Correction Parameters";
            // 
            // kryptonCheckBoxUndetectedPrecursors
            // 
            this.kryptonCheckBoxUndetectedPrecursors.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxUndetectedPrecursors.Location = new System.Drawing.Point(272, 24);
            this.kryptonCheckBoxUndetectedPrecursors.Name = "kryptonCheckBoxUndetectedPrecursors";
            this.kryptonCheckBoxUndetectedPrecursors.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxUndetectedPrecursors.Size = new System.Drawing.Size(144, 20);
            this.kryptonCheckBoxUndetectedPrecursors.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxUndetectedPrecursors.TabIndex = 31;
            this.kryptonCheckBoxUndetectedPrecursors.Text = "UndetectedPrecursors";
            this.kryptonCheckBoxUndetectedPrecursors.Values.ExtraText = "";
            this.kryptonCheckBoxUndetectedPrecursors.Values.Image = null;
            this.kryptonCheckBoxUndetectedPrecursors.Values.Text = "UndetectedPrecursors";
            // 
            // kryptonCheckBoxMS1CorrSkipPlusOnes
            // 
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Checked = true;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Location = new System.Drawing.Point(272, 56);
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Name = "kryptonCheckBoxMS1CorrSkipPlusOnes";
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Size = new System.Drawing.Size(114, 20);
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.TabIndex = 30;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Text = "Skip +1\'s in MS1";
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Values.ExtraText = "";
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Values.Image = null;
            this.kryptonCheckBoxMS1CorrSkipPlusOnes.Values.Text = "Skip +1\'s in MS1";
            // 
            // kryptonLabel13
            // 
            this.kryptonLabel13.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel13.Location = new System.Drawing.Point(71, 24);
            this.kryptonLabel13.Name = "kryptonLabel13";
            this.kryptonLabel13.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel13.Size = new System.Drawing.Size(120, 20);
            this.kryptonLabel13.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel13.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel13.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel13.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel13.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel13.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel13.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel13.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel13.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel13.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel13.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel13.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel13.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel13.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel13.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel13.TabIndex = 28;
            this.kryptonLabel13.Text = "Isolation window +-";
            this.kryptonLabel13.Values.ExtraText = "";
            this.kryptonLabel13.Values.Image = null;
            this.kryptonLabel13.Values.Text = "Isolation window +-";
            // 
            // kryptonNumericUpDownToleranceForMultipleEnvelopes
            // 
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.DecimalPlaces = 2;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.Location = new System.Drawing.Point(198, 24);
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.Name = "kryptonNumericUpDownToleranceForMultipleEnvelopes";
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.Size = new System.Drawing.Size(63, 22);
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.TabIndex = 29;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownToleranceForMultipleEnvelopes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // kryptonLabel10
            // 
            this.kryptonLabel10.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel10.Location = new System.Drawing.Point(95, 56);
            this.kryptonLabel10.Name = "kryptonLabel10";
            this.kryptonLabel10.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel10.Size = new System.Drawing.Size(96, 20);
            this.kryptonLabel10.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel10.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel10.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel10.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel10.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel10.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel10.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel10.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel10.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel10.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel10.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel10.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel10.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel10.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel10.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel10.TabIndex = 19;
            this.kryptonLabel10.Text = "Stringency MS1";
            this.kryptonLabel10.Values.ExtraText = "";
            this.kryptonLabel10.Values.Image = null;
            this.kryptonLabel10.Values.Text = "Stringency MS1";
            // 
            // kryptonNumericUpDownStringencyMS1Correction
            // 
            this.kryptonNumericUpDownStringencyMS1Correction.DecimalPlaces = 2;
            this.kryptonNumericUpDownStringencyMS1Correction.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.kryptonNumericUpDownStringencyMS1Correction.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownStringencyMS1Correction.Location = new System.Drawing.Point(198, 54);
            this.kryptonNumericUpDownStringencyMS1Correction.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kryptonNumericUpDownStringencyMS1Correction.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            65536});
            this.kryptonNumericUpDownStringencyMS1Correction.Name = "kryptonNumericUpDownStringencyMS1Correction";
            this.kryptonNumericUpDownStringencyMS1Correction.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownStringencyMS1Correction.Size = new System.Drawing.Size(67, 22);
            this.kryptonNumericUpDownStringencyMS1Correction.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1Correction.TabIndex = 20;
            this.kryptonNumericUpDownStringencyMS1Correction.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownStringencyMS1Correction.Value = new decimal(new int[] {
            90,
            0,
            0,
            131072});
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(529, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(277, 176);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // groupBoxOutputParams
            // 
            this.groupBoxOutputParams.Controls.Add(this.kryptonLabel12);
            this.groupBoxOutputParams.Controls.Add(this.kryptonCheckBoxITMS);
            this.groupBoxOutputParams.Controls.Add(this.kryptonCheckBoxFTMS);
            this.groupBoxOutputParams.Controls.Add(this.kryptonLabel8);
            this.groupBoxOutputParams.Controls.Add(this.kryptonButtonSaveParameters);
            this.groupBoxOutputParams.Controls.Add(this.kryptonNumericUpDownMinDechargedPeaks);
            this.groupBoxOutputParams.Location = new System.Drawing.Point(440, 250);
            this.groupBoxOutputParams.Name = "groupBoxOutputParams";
            this.groupBoxOutputParams.Size = new System.Drawing.Size(461, 92);
            this.groupBoxOutputParams.TabIndex = 37;
            this.groupBoxOutputParams.TabStop = false;
            this.groupBoxOutputParams.Text = "Output Parameters";
            // 
            // kryptonLabel12
            // 
            this.kryptonLabel12.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel12.Location = new System.Drawing.Point(6, 19);
            this.kryptonLabel12.Name = "kryptonLabel12";
            this.kryptonLabel12.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel12.Size = new System.Drawing.Size(94, 20);
            this.kryptonLabel12.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel12.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel12.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel12.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel12.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel12.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel12.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel12.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel12.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel12.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel12.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel12.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel12.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel12.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel12.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel12.TabIndex = 27;
            this.kryptonLabel12.Text = "Output Spectra";
            this.kryptonLabel12.Values.ExtraText = "";
            this.kryptonLabel12.Values.Image = null;
            this.kryptonLabel12.Values.Text = "Output Spectra";
            // 
            // kryptonCheckBoxITMS
            // 
            this.kryptonCheckBoxITMS.Checked = true;
            this.kryptonCheckBoxITMS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.kryptonCheckBoxITMS.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxITMS.Location = new System.Drawing.Point(158, 19);
            this.kryptonCheckBoxITMS.Name = "kryptonCheckBoxITMS";
            this.kryptonCheckBoxITMS.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxITMS.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxITMS.Size = new System.Drawing.Size(51, 20);
            this.kryptonCheckBoxITMS.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxITMS.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxITMS.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxITMS.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxITMS.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxITMS.TabIndex = 13;
            this.kryptonCheckBoxITMS.Text = "ITMS";
            this.kryptonCheckBoxITMS.Values.ExtraText = "";
            this.kryptonCheckBoxITMS.Values.Image = null;
            this.kryptonCheckBoxITMS.Values.Text = "ITMS";
            // 
            // kryptonCheckBoxFTMS
            // 
            this.kryptonCheckBoxFTMS.Checked = true;
            this.kryptonCheckBoxFTMS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.kryptonCheckBoxFTMS.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxFTMS.Location = new System.Drawing.Point(215, 19);
            this.kryptonCheckBoxFTMS.Name = "kryptonCheckBoxFTMS";
            this.kryptonCheckBoxFTMS.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTMS.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxFTMS.Size = new System.Drawing.Size(54, 20);
            this.kryptonCheckBoxFTMS.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTMS.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTMS.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxFTMS.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxFTMS.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxFTMS.TabIndex = 14;
            this.kryptonCheckBoxFTMS.Text = "FTMS";
            this.kryptonCheckBoxFTMS.Values.ExtraText = "";
            this.kryptonCheckBoxFTMS.Values.Image = null;
            this.kryptonCheckBoxFTMS.Values.Text = "FTMS";
            // 
            // kryptonLabel8
            // 
            this.kryptonLabel8.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel8.Location = new System.Drawing.Point(6, 56);
            this.kryptonLabel8.Name = "kryptonLabel8";
            this.kryptonLabel8.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel8.Size = new System.Drawing.Size(210, 20);
            this.kryptonLabel8.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel8.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel8.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel8.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel8.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel8.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel8.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel8.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel8.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel8.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel8.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel8.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel8.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel8.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel8.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel8.TabIndex = 10;
            this.kryptonLabel8.Text = "Min deconvoluted peaks /  spectrum";
            this.kryptonLabel8.Values.ExtraText = "";
            this.kryptonLabel8.Values.Image = null;
            this.kryptonLabel8.Values.Text = "Min deconvoluted peaks /  spectrum";
            // 
            // groupBoxEnvelopeDetectionParameters
            // 
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonCheckBoxSkipPlusOneInMS1);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonCheckBoxSkipPlusOnesInMS2);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonNumericUpDownStringencyMS1);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonLabel14);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonLabel3);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonLabel4);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonNumericUpDownStringencyMS2);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonNumericUpDownPPM);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonNumericUpDownMaxCharge);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonLabel6);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonLabel7);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonNumericUpDownConvolutedClusteringFacotr);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonLabel9);
            this.groupBoxEnvelopeDetectionParameters.Controls.Add(this.kryptonNumericUpDownDeconvolutedClusteringFactor);
            this.groupBoxEnvelopeDetectionParameters.Location = new System.Drawing.Point(20, 126);
            this.groupBoxEnvelopeDetectionParameters.Name = "groupBoxEnvelopeDetectionParameters";
            this.groupBoxEnvelopeDetectionParameters.Size = new System.Drawing.Size(414, 99);
            this.groupBoxEnvelopeDetectionParameters.TabIndex = 36;
            this.groupBoxEnvelopeDetectionParameters.TabStop = false;
            this.groupBoxEnvelopeDetectionParameters.Text = "Envelope Detection Parameters";
            // 
            // kryptonCheckBoxSkipPlusOneInMS1
            // 
            this.kryptonCheckBoxSkipPlusOneInMS1.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxSkipPlusOneInMS1.Location = new System.Drawing.Point(132, 73);
            this.kryptonCheckBoxSkipPlusOneInMS1.Name = "kryptonCheckBoxSkipPlusOneInMS1";
            this.kryptonCheckBoxSkipPlusOneInMS1.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxSkipPlusOneInMS1.Size = new System.Drawing.Size(114, 20);
            this.kryptonCheckBoxSkipPlusOneInMS1.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOneInMS1.TabIndex = 22;
            this.kryptonCheckBoxSkipPlusOneInMS1.Text = "Skip +1\'s in MS1";
            this.kryptonCheckBoxSkipPlusOneInMS1.Values.ExtraText = "";
            this.kryptonCheckBoxSkipPlusOneInMS1.Values.Image = null;
            this.kryptonCheckBoxSkipPlusOneInMS1.Values.Text = "Skip +1\'s in MS1";
            // 
            // kryptonCheckBoxSkipPlusOnesInMS2
            // 
            this.kryptonCheckBoxSkipPlusOnesInMS2.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonCheckBoxSkipPlusOnesInMS2.Location = new System.Drawing.Point(12, 73);
            this.kryptonCheckBoxSkipPlusOnesInMS2.Name = "kryptonCheckBoxSkipPlusOnesInMS2";
            this.kryptonCheckBoxSkipPlusOnesInMS2.OverrideFocus.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.OverrideFocus.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.OverrideFocus.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.OverrideFocus.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.OverrideFocus.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonCheckBoxSkipPlusOnesInMS2.Size = new System.Drawing.Size(114, 20);
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonCheckBoxSkipPlusOnesInMS2.TabIndex = 21;
            this.kryptonCheckBoxSkipPlusOnesInMS2.Text = "Skip +1\'s in MS2";
            this.kryptonCheckBoxSkipPlusOnesInMS2.Values.ExtraText = "";
            this.kryptonCheckBoxSkipPlusOnesInMS2.Values.Image = null;
            this.kryptonCheckBoxSkipPlusOnesInMS2.Values.Text = "Skip +1\'s in MS2";
            // 
            // kryptonNumericUpDownStringencyMS1
            // 
            this.kryptonNumericUpDownStringencyMS1.DecimalPlaces = 2;
            this.kryptonNumericUpDownStringencyMS1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.kryptonNumericUpDownStringencyMS1.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownStringencyMS1.Location = new System.Drawing.Point(326, 17);
            this.kryptonNumericUpDownStringencyMS1.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kryptonNumericUpDownStringencyMS1.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.kryptonNumericUpDownStringencyMS1.Name = "kryptonNumericUpDownStringencyMS1";
            this.kryptonNumericUpDownStringencyMS1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownStringencyMS1.Size = new System.Drawing.Size(67, 22);
            this.kryptonNumericUpDownStringencyMS1.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS1.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS1.TabIndex = 20;
            this.kryptonNumericUpDownStringencyMS1.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownStringencyMS1.Value = new decimal(new int[] {
            90,
            0,
            0,
            131072});
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel3.Location = new System.Drawing.Point(6, 19);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel3.Size = new System.Drawing.Size(35, 20);
            this.kryptonLabel3.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel3.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel3.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel3.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel3.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel3.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel3.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel3.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel3.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel3.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel3.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel3.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel3.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel3.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel3.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel3.TabIndex = 2;
            this.kryptonLabel3.Text = "PPM";
            this.kryptonLabel3.Values.ExtraText = "";
            this.kryptonLabel3.Values.Image = null;
            this.kryptonLabel3.Values.Text = "PPM";
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel4.Location = new System.Drawing.Point(12, 45);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel4.Size = new System.Drawing.Size(53, 20);
            this.kryptonLabel4.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel4.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel4.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel4.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel4.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel4.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel4.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel4.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel4.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel4.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel4.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel4.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel4.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel4.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel4.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel4.TabIndex = 3;
            this.kryptonLabel4.Text = "Str MS2";
            this.kryptonLabel4.Values.ExtraText = "";
            this.kryptonLabel4.Values.Image = null;
            this.kryptonLabel4.Values.Text = "Str MS2";
            // 
            // kryptonNumericUpDownStringencyMS2
            // 
            this.kryptonNumericUpDownStringencyMS2.DecimalPlaces = 2;
            this.kryptonNumericUpDownStringencyMS2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.kryptonNumericUpDownStringencyMS2.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownStringencyMS2.Location = new System.Drawing.Point(71, 45);
            this.kryptonNumericUpDownStringencyMS2.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kryptonNumericUpDownStringencyMS2.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.kryptonNumericUpDownStringencyMS2.Name = "kryptonNumericUpDownStringencyMS2";
            this.kryptonNumericUpDownStringencyMS2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownStringencyMS2.Size = new System.Drawing.Size(54, 22);
            this.kryptonNumericUpDownStringencyMS2.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS2.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS2.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS2.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS2.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS2.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS2.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownStringencyMS2.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownStringencyMS2.TabIndex = 4;
            this.kryptonNumericUpDownStringencyMS2.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownStringencyMS2.Value = new decimal(new int[] {
            80,
            0,
            0,
            131072});
            // 
            // kryptonNumericUpDownPPM
            // 
            this.kryptonNumericUpDownPPM.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownPPM.Location = new System.Drawing.Point(71, 19);
            this.kryptonNumericUpDownPPM.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.kryptonNumericUpDownPPM.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.kryptonNumericUpDownPPM.Name = "kryptonNumericUpDownPPM";
            this.kryptonNumericUpDownPPM.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownPPM.Size = new System.Drawing.Size(54, 22);
            this.kryptonNumericUpDownPPM.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownPPM.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownPPM.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownPPM.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownPPM.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownPPM.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownPPM.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownPPM.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownPPM.TabIndex = 5;
            this.kryptonNumericUpDownPPM.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownPPM.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // kryptonNumericUpDownMaxCharge
            // 
            this.kryptonNumericUpDownMaxCharge.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownMaxCharge.Location = new System.Drawing.Point(211, 19);
            this.kryptonNumericUpDownMaxCharge.Maximum = new decimal(new int[] {
            21,
            0,
            0,
            0});
            this.kryptonNumericUpDownMaxCharge.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kryptonNumericUpDownMaxCharge.Name = "kryptonNumericUpDownMaxCharge";
            this.kryptonNumericUpDownMaxCharge.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownMaxCharge.Size = new System.Drawing.Size(50, 22);
            this.kryptonNumericUpDownMaxCharge.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxCharge.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxCharge.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxCharge.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxCharge.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxCharge.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxCharge.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownMaxCharge.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownMaxCharge.TabIndex = 8;
            this.kryptonNumericUpDownMaxCharge.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownMaxCharge.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel6.Location = new System.Drawing.Point(129, 19);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel6.Size = new System.Drawing.Size(76, 20);
            this.kryptonLabel6.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel6.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel6.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel6.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel6.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel6.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel6.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel6.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel6.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel6.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel6.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel6.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel6.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel6.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel6.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel6.TabIndex = 9;
            this.kryptonLabel6.Text = "Max Charge";
            this.kryptonLabel6.Values.ExtraText = "";
            this.kryptonLabel6.Values.Image = null;
            this.kryptonLabel6.Values.Text = "Max Charge";
            // 
            // kryptonLabel7
            // 
            this.kryptonLabel7.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel7.Location = new System.Drawing.Point(129, 45);
            this.kryptonLabel7.Name = "kryptonLabel7";
            this.kryptonLabel7.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel7.Size = new System.Drawing.Size(76, 20);
            this.kryptonLabel7.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel7.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel7.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel7.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel7.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel7.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel7.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel7.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel7.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel7.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel7.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel7.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel7.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel7.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel7.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel7.TabIndex = 15;
            this.kryptonLabel7.Text = "Clust Decon";
            this.kryptonLabel7.Values.ExtraText = "";
            this.kryptonLabel7.Values.Image = null;
            this.kryptonLabel7.Values.Text = "Clust Decon";
            // 
            // kryptonNumericUpDownConvolutedClusteringFacotr
            // 
            this.kryptonNumericUpDownConvolutedClusteringFacotr.DecimalPlaces = 2;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.kryptonNumericUpDownConvolutedClusteringFacotr.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.Location = new System.Drawing.Point(326, 45);
            this.kryptonNumericUpDownConvolutedClusteringFacotr.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.kryptonNumericUpDownConvolutedClusteringFacotr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.kryptonNumericUpDownConvolutedClusteringFacotr.Name = "kryptonNumericUpDownConvolutedClusteringFacotr";
            this.kryptonNumericUpDownConvolutedClusteringFacotr.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.Size = new System.Drawing.Size(67, 22);
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.TabIndex = 18;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownConvolutedClusteringFacotr.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // kryptonLabel9
            // 
            this.kryptonLabel9.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl;
            this.kryptonLabel9.Location = new System.Drawing.Point(267, 45);
            this.kryptonLabel9.Name = "kryptonLabel9";
            this.kryptonLabel9.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonLabel9.Size = new System.Drawing.Size(41, 20);
            this.kryptonLabel9.StateCommon.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel9.StateCommon.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel9.StateCommon.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel9.StateCommon.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel9.StateCommon.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel9.StateDisabled.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel9.StateDisabled.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel9.StateDisabled.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel9.StateDisabled.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel9.StateDisabled.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel9.StateNormal.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Inherit;
            this.kryptonLabel9.StateNormal.LongText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel9.StateNormal.LongText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel9.StateNormal.ShortText.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonLabel9.StateNormal.ShortText.Trim = ComponentFactory.Krypton.Toolkit.PaletteTextTrim.Inherit;
            this.kryptonLabel9.TabIndex = 16;
            this.kryptonLabel9.Text = "Conv.";
            this.kryptonLabel9.Values.ExtraText = "";
            this.kryptonLabel9.Values.Image = null;
            this.kryptonLabel9.Values.Text = "Conv.";
            // 
            // kryptonNumericUpDownDeconvolutedClusteringFactor
            // 
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.DecimalPlaces = 2;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Standalone;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.Location = new System.Drawing.Point(211, 47);
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.Name = "kryptonNumericUpDownDeconvolutedClusteringFactor";
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Global;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.Size = new System.Drawing.Size(50, 22);
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateActive.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateActive.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateCommon.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateDisabled.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateDisabled.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateNormal.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.StateNormal.Border.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.TabIndex = 17;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.UpDownButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.InputControl;
            this.kryptonNumericUpDownDeconvolutedClusteringFactor.Value = new decimal(new int[] {
            14,
            0,
            0,
            65536});
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // kryptonCheckSetHowToDo
            // 
            this.kryptonCheckSetHowToDo.CheckButtons.Add(this.kryptonCheckButtonBottumUp);
            this.kryptonCheckSetHowToDo.CheckButtons.Add(this.kryptonCheckButtonMiddleDown);
            this.kryptonCheckSetHowToDo.CheckButtons.Add(this.kryptonCheckButtonGeekMode);
            this.kryptonCheckSetHowToDo.CheckButtons.Add(this.kryptonCheckButtonTopDonw);
            this.kryptonCheckSetHowToDo.CheckedButton = this.kryptonCheckButtonBottumUp;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(909, 454);
            this.tabControl1.TabIndex = 18;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.kryptonSplitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(901, 428);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Spectra processor";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBoxControlPanel);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(901, 428);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Control Panel";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.isotopicFinder2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(901, 428);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Spectrum Deisotoping Toy";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.envelopeIntensityGenerator3);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(901, 428);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Envelope Signal Toy";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // isotopicFinder2
            // 
            this.isotopicFinder2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isotopicFinder2.Location = new System.Drawing.Point(0, 0);
            this.isotopicFinder2.Name = "isotopicFinder2";
            this.isotopicFinder2.Size = new System.Drawing.Size(901, 428);
            this.isotopicFinder2.TabIndex = 0;
            this.isotopicFinder2.Load += new System.EventHandler(this.isotopicFinder2_Load);
            // 
            // envelopeIntensityGenerator3
            // 
            this.envelopeIntensityGenerator3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.envelopeIntensityGenerator3.Location = new System.Drawing.Point(0, 0);
            this.envelopeIntensityGenerator3.Name = "envelopeIntensityGenerator3";
            this.envelopeIntensityGenerator3.Size = new System.Drawing.Size(901, 428);
            this.envelopeIntensityGenerator3.TabIndex = 0;
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 454);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GUI";
            this.Text = "Y.A.D.A. (Beta) - A tool for taking the most out of high resolution mass spectra." +
    " To be used on middle-down or bottum-up experiments.";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxWhatToDo.ResumeLayout(false);
            this.groupBoxWhatToDo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxNoThreads)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonSplitContainer1.Panel1)).EndInit();
            this.kryptonSplitContainer1.Panel1.ResumeLayout(false);
            this.kryptonSplitContainer1.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonSplitContainer1.Panel2)).EndInit();
            this.kryptonSplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonSplitContainer1)).EndInit();
            this.kryptonSplitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxStep2HowToDo.ResumeLayout(false);
            this.groupBoxControlPanel.ResumeLayout(false);
            this.groupBoxPeakFilteringParameters.ResumeLayout(false);
            this.groupBoxPeakFilteringParameters.PerformLayout();
            this.groupBoxMS1CorrectionParameters.ResumeLayout(false);
            this.groupBoxMS1CorrectionParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBoxOutputParams.ResumeLayout(false);
            this.groupBoxOutputParams.PerformLayout();
            this.groupBoxEnvelopeDetectionParameters.ResumeLayout(false);
            this.groupBoxEnvelopeDetectionParameters.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonFeelingLucky;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox groupBox3;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox kryptonTextBoxInputDir;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonOutputDir;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox kryptonTextBoxOutputDir;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonInputDir;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonDDGo;
        private System.Windows.Forms.GroupBox groupBoxWhatToDo;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton kryptonRadioButtonDDMS2usingMS1;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton kryptonRadioButtonDDMS1andMS2;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton kryptonRadioButtonDDMS2;
        private PatternTools.MSParser.Deisotoping.IsotopicFinder isotopicFinder1;
        private PatternTools.MSParser.Deisotoping.EnvelopeIntensityGenerator envelopeIntensityGenerator1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private PatternTools.MSParser.Deisotoping.SpectrumFilter spectrumFilter1;
        private System.Windows.Forms.GroupBox groupBoxMS1CorrectionParameters;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel13;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownToleranceForMultipleEnvelopes;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel10;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownStringencyMS1Correction;
        private System.Windows.Forms.GroupBox groupBoxOutputParams;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel12;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxITMS;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxFTMS;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel8;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonSaveParameters;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMinDechargedPeaks;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel11;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMaxNoPeaks;
        private System.Windows.Forms.GroupBox groupBoxEnvelopeDetectionParameters;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownStringencyMS1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel14;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownStringencyMS2;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownPPM;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMaxCharge;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel7;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownConvolutedClusteringFacotr;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel9;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownDeconvolutedClusteringFactor;
        private System.Windows.Forms.GroupBox groupBoxPeakFilteringParameters;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMS1MinIntensityz;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownMS2MinIntensity;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown kryptonNumericUpDownFilteringPPM;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxFTFiltering;
        private System.Windows.Forms.PictureBox pictureBox2;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer kryptonSplitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxControlPanel;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxMS1CorrSkipPlusOnes;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxGaussianFilter;
        //private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage1;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxUndetectedPrecursors;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton kryptonCheckButtonGeekMode;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton kryptonCheckButtonMiddleDown;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton kryptonCheckButtonBottumUp;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckSet kryptonCheckSetHowToDo;
        private System.Windows.Forms.GroupBox groupBoxStep2HowToDo;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton kryptonCheckButtonTopDonw;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxSkipPlusOnesInMS2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButtonAbout;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxPreserveUniqueXtractResults;
        private PatternTools.MSParser.Deisotoping.SpectrumFilter spectrumFilter2;
        private PatternTools.MSParser.Deisotoping.IsotopicFinder isotopicFinder2;
        private PatternTools.MSParser.Deisotoping.EnvelopeIntensityGenerator envelopeIntensityGenerator2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonFormatMGF;
        private System.Windows.Forms.RadioButton radioButtonFormatMS1MS2;
        private PatternTools.MSParser.Deisotoping.EnvelopeIntensityGenerator envelopeIntensityGenerator3;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton kryptonRadioButtonDDMS1;
        private System.Windows.Forms.CheckBox checkBoxDeconvoluteMS2;
        private System.Windows.Forms.CheckBox checkBoxDecharge;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxNoThreads;
        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox kryptonCheckBoxSkipPlusOneInMS1;
    }
}