﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Soap;

namespace YADA
{
    /// <summary>
    /// Encapsulates all execution parameters
    /// </summary>
    [Serializable]
    public struct ExecutionParams
    {
        public int MaxNoThreads { get; set; }
        public bool SkipPlusOneInMS1 { get; set; }
        public double MS1MinIntensity { get; set; }
        public double MS2MinIntensity { get; set; }
        public double PPM { get; set; }
        public int MaxCharge { get; set; }
        public double StringencyMS1 { get; set; }
        public double StringencyMS2 { get; set; }
        public double StringencyMS1Correction { get; set; }
        public int MinDechargedPeaks { get; set; }
        public bool PrintITMS { get; set; }
        public bool PrintFTMS { get; set; }
        public double DeconvolutedClusteringFactor { get; set; }
        public double ConvolutedClusteringFactor { get; set; }
        public int MaxNoPeaks { get; set; }
        public bool FTMSFiltering { get; set; }
        public double FTMSFilteringPPM { get; set; }
        public double ToleranceMultiplexCorrection { get; set; }
        public bool SkipPlusOnesMS1Corr { get; set; }
        public bool GaussianFilter { get; set; }
        public bool UndetectedPrecursors { get; set; }
        public bool SkipPlusOnesInMS2 { get; set; }
        public bool PreserveUniqueXtractResults { get; set; }
        public bool FormatIsMS1MS2 { get; set; }
        public bool DeconvoluteMS2 { get; set; }


        public void SetMyParams(double MS1MinIntensity, double MS2MinIntensity,
            double PPM, int MaxCharge, int MinDechargedPeaks,
            bool PrintFTMS, bool PrintITMS, double stringencyMS1,
            double stringencyMS2, double convolutedClusteringFactor,
            double deconvolutedClusteringFactor, double stringencyMS1Correction,
            int maxNoPeaks, bool onlyLocalMaximumPeaks, double ftmsfilteringppm,
            double toleranceForMultipleEnvelopes, bool skipPlusOnesMS1Corr, bool GaussianFilter,
            bool undetectedPrecursors, bool skipPlusOnesInMS2, bool preserveUniqueXtractResults,
            bool formatIsMS1MS2, int maxNoThreads, bool skipPlusOnesInMS1, bool deconvoluteMS2
            )
        {
            this.MS1MinIntensity = MS1MinIntensity;
            this.MS2MinIntensity = MS2MinIntensity;
            this.PPM = PPM;
            this.MaxCharge = MaxCharge;
            this.MinDechargedPeaks = MinDechargedPeaks;
            this.PrintITMS = PrintITMS;
            this.PrintFTMS = PrintFTMS;
            this.StringencyMS1 = stringencyMS1;
            this.StringencyMS2 = stringencyMS2;
            this.DeconvolutedClusteringFactor = deconvolutedClusteringFactor;
            this.ConvolutedClusteringFactor = convolutedClusteringFactor;
            this.StringencyMS1Correction = stringencyMS1Correction;
            this.MaxNoPeaks = maxNoPeaks;
            this.FTMSFiltering = onlyLocalMaximumPeaks;
            this.FTMSFilteringPPM = ftmsfilteringppm;
            this.ToleranceMultiplexCorrection = toleranceForMultipleEnvelopes;
            this.SkipPlusOnesMS1Corr = skipPlusOnesMS1Corr;
            this.GaussianFilter = GaussianFilter;
            this.UndetectedPrecursors = undetectedPrecursors;
            this.SkipPlusOnesInMS2 = skipPlusOnesInMS2;
            this.PreserveUniqueXtractResults = preserveUniqueXtractResults;
            this.FormatIsMS1MS2 = formatIsMS1MS2;
            this.SkipPlusOneInMS1 = skipPlusOnesInMS1;
            this.MaxNoThreads = maxNoThreads;
            this.DeconvoluteMS2 = deconvoluteMS2;
        }

        public void SaveParams (string fileName) {
                        //Serialize it!
            System.IO.FileStream flStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            SoapFormatter sf = new SoapFormatter();
            sf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            sf.TypeFormat = System.Runtime.Serialization.Formatters.FormatterTypeStyle.TypesAlways;
            sf.Serialize(flStream, this);
            flStream.Close();
        }

        public void LoadFromFile(string fileName)
        {
            //Serialize it!
            System.IO.FileStream flStream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            SoapFormatter sf = new SoapFormatter();
            this = (ExecutionParams)sf.Deserialize(flStream);
            flStream.Close();
        }

        internal void LoadBottomUpDefaults()
        {
            this.MS1MinIntensity = 10000;
            this.MS2MinIntensity = 1;
            this.PPM = 8;
            this.MaxCharge = 6;
            this.MinDechargedPeaks = 10;
            this.PrintITMS = true;
            this.PrintFTMS = true;
            this.StringencyMS1 = 0.95;
            this.StringencyMS2 = 0.85;
            this.DeconvolutedClusteringFactor = 1;
            this.ConvolutedClusteringFactor = 0.2;
            this.StringencyMS1Correction = 0.92;
            this.MaxNoPeaks = 1500;
            this.FTMSFiltering = true;
            this.FTMSFilteringPPM = 30;
            this.ToleranceMultiplexCorrection = 0.3;
            this.SkipPlusOnesMS1Corr = true;
            this.SkipPlusOneInMS1 = true;
            this.GaussianFilter = false;
            this.UndetectedPrecursors = false;
            this.SkipPlusOnesInMS2 = false;
            this.PreserveUniqueXtractResults = false;
        }

        internal void LoadMiddleDownDefaults()
        {
            this.MS1MinIntensity = 10000;
            this.MS2MinIntensity = 1;
            this.PPM = 8;
            this.MaxCharge = 10;
            this.MinDechargedPeaks = 10;
            this.PrintITMS = true;
            this.PrintFTMS = true;
            this.StringencyMS1 = 0.90;
            this.StringencyMS2 = 0.85;
            this.DeconvolutedClusteringFactor = 1;
            this.ConvolutedClusteringFactor = 0.18;
            this.StringencyMS1Correction = 0.92;
            this.MaxNoPeaks = 2000;
            this.FTMSFiltering = true;
            this.FTMSFilteringPPM = 30;
            this.ToleranceMultiplexCorrection = 0.4;
            this.SkipPlusOnesMS1Corr = true;
            this.GaussianFilter = false;
            this.UndetectedPrecursors = false;
            this.SkipPlusOnesInMS2 = false;
            this.SkipPlusOneInMS1 = true;
            this.PreserveUniqueXtractResults = false;
        }

        internal void LoadTopDownpDefaults()
        {
            this.MS1MinIntensity = 10000;
            this.MS2MinIntensity = 1;
            this.PPM = 15;
            this.MaxCharge = 21;
            this.MinDechargedPeaks = 10;
            this.PrintITMS = false;
            this.PrintFTMS = true;
            this.StringencyMS1 = 0.95;
            this.StringencyMS2 = 0.90;
            this.DeconvolutedClusteringFactor = 0.92;
            this.ConvolutedClusteringFactor = 0.18;
            this.StringencyMS1Correction = 0.92;
            this.MaxNoPeaks = 3000;
            this.FTMSFiltering = true;
            this.FTMSFilteringPPM = 30;
            this.ToleranceMultiplexCorrection = 1.5;
            this.SkipPlusOnesMS1Corr = true;
            this.GaussianFilter = true;
            this.UndetectedPrecursors = true;
            this.SkipPlusOneInMS1 = true;
            this.SkipPlusOnesInMS2 = true;
            this.PreserveUniqueXtractResults = true;
        }

        public bool Decharged { get; set; }
    }
}
