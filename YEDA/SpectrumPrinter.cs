﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatternTools.MSParser;
using System.IO;
using System.Text.RegularExpressions;

namespace YADA
{
    public static class SpectrumPrinter
    {

        public static void WriteFileHeader(StreamWriter outputFile, ExecutionParams eParams, string header)
        {
            outputFile.WriteLine("H\tYADA");
            outputFile.WriteLine("H\tPaulo C Carvalho & Tao Xu");
            outputFile.WriteLine("H\tExecution Time: " + System.DateTime.Now.ToString());
            outputFile.WriteLine("H\tPPM: " + eParams.PPM);
            outputFile.WriteLine("H\tDecharged: " + eParams.Decharged);
            outputFile.WriteLine("H\tStringency MS1: " + eParams.StringencyMS1);
            outputFile.WriteLine("H\tStringency MS2: " + eParams.StringencyMS2);
            outputFile.WriteLine("H\tStringency Monoisotopic Assignment: " + eParams.StringencyMS1Correction);
            outputFile.WriteLine("H\tTolerance for multiple envelopes: " + eParams.ToleranceMultiplexCorrection);
            outputFile.WriteLine("H\tMaxCharge: " + eParams.MaxCharge);
            outputFile.WriteLine("H\tConvoluted clustering Factor: " + eParams.ConvolutedClusteringFactor);
            outputFile.WriteLine("H\tDeconvoluted clustering Factor: " + eParams.DeconvolutedClusteringFactor);
            outputFile.WriteLine("H\tMax No of Peaks: " + eParams.MaxNoPeaks);
            outputFile.WriteLine("H\tMin MS1 Ion Intensity: " + eParams.MS1MinIntensity);
            outputFile.WriteLine("H\tMin MS2 Ion Intensity: " + eParams.MS2MinIntensity);
            outputFile.WriteLine("H\tOutput FTMS:" + eParams.PrintFTMS);
            outputFile.WriteLine("H\tOutput ITMS:" + eParams.PrintITMS);
            
            string[] lines = Regex.Split(header, "\n");
            foreach (string line in lines)
            {
                if (!line.Equals(""))
                {
                    outputFile.WriteLine(line);
                }
            }
            
        }


        public static void PrintTheSpectrum(StreamWriter outputFile, PatternTools.MSParser.MSFull ms, ExecutionParams eParams)
        {
            //Some basic checking before we print anything
            if (ms.MSData.Count < eParams.MinDechargedPeaks)
            {
                Console.WriteLine("Skipping scan " + ms.ScanNumber + " for it has less than " + eParams.MinDechargedPeaks + " peaks.");
                return;
            }

            if (eParams.FormatIsMS1MS2)
            {

                outputFile.WriteLine("S\t" + ms.ScanNumber + "\t" + ms.ScanNumber + "\t" + ms.ChargedPrecursor);
                outputFile.WriteLine("I\tRetTime\t" + ms.CromatographyRetentionTime);
                outputFile.WriteLine("I\tIonInjectionTime\t" + ms.IonInjectionTime);
                outputFile.WriteLine("I\tActivationType\t" + ms.ActivationType);
                outputFile.WriteLine("I\tInstrumentType\t" + ms.InstrumentType);

                //Write the YADA decharged lines
                foreach (string i in ms.Ilines)
                {
                    outputFile.WriteLine(i);
                }


                //We need to print the Z line only if it is an MS2
                if (ms.isMS2 && ms.ZLines.Count > 0)
                {
                    foreach (string ZLine in ms.ZLines)
                    {
                        //Make sure there are no +1...
                        string[] cols = Regex.Split(ZLine, "\t");
                        if (int.Parse(cols[1]) == 1 && !ms.isMS2 && eParams.SkipPlusOnesMS1Corr)
                        {
                            continue;
                        }

                        outputFile.WriteLine(ZLine);
                    }
                }

                foreach (Ion ion in ms.MSData)
                {
                    outputFile.WriteLine(ion.MZ + " " + ion.Intensity);
                }
            }
            else
            {
                if (ms.isMS2 && ms.ZLines.Count > 0)
                {
                    foreach (string ZLine in ms.ZLines)
                    {
                        //Make sure there are no +1...
                        string[] cols = Regex.Split(ZLine, "\t");
                        if (int.Parse(cols[1]) == 1 && !ms.isMS2 && eParams.SkipPlusOnesMS1Corr)
                        {
                            continue;
                        }

                        //Write the mass spectrum
                        //Lets print in MGF
                        outputFile.WriteLine("BEGIN IONS");
                        outputFile.WriteLine("RTINSECONDS=" + ms.CromatographyRetentionTime);
                        outputFile.WriteLine("SCANS=" + ms.ScanNumber);
                        outputFile.WriteLine("INSTRUMENT=" + ms.InstrumentType);

                        double pepmass = double.Parse(cols[2]);
                        outputFile.WriteLine("PEPMASS=" + Math.Round(pepmass - 1.007276466, 4));
                        outputFile.WriteLine("CHARGE="+cols[1]+"+");

                        foreach (Ion ion in ms.MSData)
                        {
                            outputFile.WriteLine(ion.MZ + "\t" + ion.Intensity);
                        }

                        outputFile.WriteLine("END IONS\n");
                    }
                }

            }
        }
    }
}
