﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using PatternTools.MSParser;
using PatternTools.MSParser.Deisotoping;

namespace YADA
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Y.A.D.A.\n");

            string instructions = "These are the command line options:\n";
            instructions += "-i input directory (required)\n";
            instructions += "-o output directory (required)\n";
            instructions += "-p parameter file (optional)\n";
            instructions += "-d decharge (1 for true)\n";
            instructions += "-c Command {1,2,3} (Choose only one - required)\n";
            instructions += " c = 1: Deisotope and decharge MS2, use MS1 for monoisotopic assignment\n";
            instructions += " c = 2: Deisotope MS2\n";
            instructions += " c = 3: Deisotope MS1 & MS2\n\n";


            PatternTools.CommandLineParser clp = new PatternTools.CommandLineParser(args);

            if (args.Length > 0)
            {
                try{ 
                Decharger decharger = new Decharger();
                ExecutionParams ep = new ExecutionParams();

                //the parameters
                string p = clp.GetParam("p");
                string i = clp.GetParam("i");
                string o = clp.GetParam("o");
                string c = clp.GetParam("c");
                bool decharge = true;

                if (clp.GetParam("d") != "1")
                {
                    decharge = false;
                }

                if (!p.Equals("NotFound"))
                {
                    Console.WriteLine("Loading parameter file: " + clp.GetParam("p"));
                    ep.LoadFromFile(clp.GetParam("p"));
                }
                else
                {
                    Console.WriteLine("Loadind default parameters");
                    ep.LoadBottomUpDefaults();
                }

                if (clp.GetParam("i").Equals("NotFound") || clp.GetParam("o").Equals("NotFound") || clp.GetParam("c").Equals("NotFound") || (clp.GetParam("i").Equals(clp.GetParam("o")) && clp.GetParam("i").Equals("NotFound")))
                {
                    Console.WriteLine(instructions);
                    return;
                }

                if (clp.GetParam("c").Equals("3"))
                {
                    //DD only MS2
                    decharger.DechargeFileExecuteOption1or2(ep, i, o, true, true, decharge);


                }
                else if (clp.GetParam("c").Equals("2"))
                {
                    decharger.DechargeFileExecuteOption1or2(ep, i, o, false, true, decharge);
                }
                else if (clp.GetParam("c").Equals("1"))
                {
                    decharger.DechargeFileExecuteOption1(ep, DeisotopeTools.GetMSFilePairs(clp.GetParam("i")), clp.GetParam("o"), true);
                }
                else
                {
                    Console.WriteLine("There seems to be an error with the command option.");
                    return;
                }
                } catch (Exception e)
                    {
                        Console.WriteLine("Error detected:");
                        Console.WriteLine("Source:" + e.Source);
                        Console.WriteLine(e.InnerException + "\n");
                        Console.WriteLine("Additional Error information \n\n" + e.Message);

                        foreach (KeyValuePair<string, string> kvp in e.Data)
                        {
                            Console.WriteLine(kvp.Key + ":" + kvp.Value);
                        }
                    }
            }
            else
            {
                GUI g = new GUI();
                g.ShowDialog();
            }
            
        }
        

    }
}
