﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatternTools.MSParser;

namespace YADA
{
    public class DeconvolutedPeakCluster
    {
        public DeconvolutedPeakCluster(double clusterCenter, List<EnvelopeScore> members)
        {
            Members = members;
            this.PrecursorCandidate = clusterCenter;
        }
        public double PrecursorCandidate { get; set; }
        public List<EnvelopeScore> Members { get; set; }
    }
}
